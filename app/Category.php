<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Category extends Model{
  protected $table = 'categories';

  public function product(){
    return $this->hasMany('App\Product');
  }

  public function getCategories(){
    $categories = DB::table($this->table)->select('id', 'parent_category', 'name', 'details', 'icon')->where('status', true)->get();
    return $categories;
  }
}
