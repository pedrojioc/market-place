<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Country extends Model{
  protected $table = 'countries';

  public function shipping(){
    return $this->belongsTo('App\Shipping');
  }

  public function customer(){
    return $this->hasMany('App\Customer');
  }

  public static function getCountries(){
    $countries = DB::table('countries')->select('*')->get();

    return $countries;
  }
}
