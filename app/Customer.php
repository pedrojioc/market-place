<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Customer extends Model{

  public function user(){
    return $this->hasOne('App\User');
  }

  public function country(){
    return $this->belongsTo('App\Country');
  }
  public function state(){
    return $this->belongsTo('App\State');
  }

  public function create($data){

    $result = DB::table('customers')->insertGetId(
      [
        'email'       => $data['email'],
        'first_name'  => $data['first_name'],
        'last_name'   => $data['last_name'],
        'country_id'  => $data['country'],
        'state_id'    => $data['state'],
        'city'        => $data['city'],
        'address'     => $data['address'],
        'postal_code' => $data['postal_code']
      ]
    );

    return $result;
  }
}
