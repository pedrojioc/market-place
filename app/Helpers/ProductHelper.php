<?php
/**
 *
 */
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use App\Package;

class ProductHelper{


  public static function checkDiscount(){
    if(!Auth::check()){
      return NULL;
    }
    if(Auth::user()->is_member_grunem){
      $package = Package::find(Auth::user()->package_id);
      $discount = ($package->discount/100);

    }else{
      $discount = NULL;
    }
    return $discount;
  }

  public static function round_up ($value, $places=0) {
    if ($places < 0) { $places = 0; }
    $mult = pow(10, $places);
    return ceil($value * $mult) / $mult;
  }

}
