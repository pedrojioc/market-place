<?php
/**
 *
 */
namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use App\Package;
use App\Customer;

class UserHelper{


  public static function profileComplete(){
    $profile_complete = true;

    if(Auth::check()){
      $customer = Customer::find(Auth::user()->customer_id);
      if(empty($customer->country_id) || empty($customer->state_id)){
        $profile_complete = false;
      }
      if(empty($customer->city) || empty($customer->address) || empty($customer->postal_code)){
        $profile_complete = false;
      }
    }

    return $profile_complete;
  }

}
