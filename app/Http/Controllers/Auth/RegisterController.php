<?php

namespace App\Http\Controllers\Auth;


use App\Notifications\UserRegisteredSuccessfully;
use App\User;
use App\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Country;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm(Request $request){
      $countries = Country::getCountries();
      if(isset($request->sponsor)){
        $sponsor = $request->sponsor;
        $exist_user = User::where('username', $sponsor)->count();
        if($exist_user == 0){
          return redirect('/');
        }
      }else{
        $sponsor = 0;
      }
      //dd($request->sponsor);
      return view('auth.register', ['countries' => $countries, 'sponsor' => $sponsor])->render();
    }

    public function register(Request $request){
      $customer = new Customer;
      //dd($request);
      $validate_data = $request->validate([
        'sponsor'     => 'required',
        'email'       => 'required|string|email|max:255|unique:users',
        'first_name'  => 'required|string|max:255',
        'last_name'   => 'required|string|max:255',
        'country'     => 'required|not_in:0',
        'state'       => 'required|not_in:0',
        'city'        => 'required|string|max:255',
        'address'     => 'required|string|max:255',
        'postal_code' => 'required|string|max:255',
        'password'    => 'required|string|min:6|confirmed',
      ]);

      try {
        $validate_data['password'] = Hash::make(array_get($validate_data, 'password'));
        $validate_data['activation_code'] = str_random(30).time();

        $customer_id = $customer->create($validate_data);
        $validate_data['customer_id'] = $customer_id;
        $validate_data['name'] = $validate_data['first_name']." ".$validate_data['last_name'];

        $user = app(User::class)->create($validate_data);
      } catch (\Exception $exception) {
        logger()->error($exception);
        return redirect()->back()->with('message', 'Unable to create new user.');
      }
      $user->notify(new UserRegisteredSuccessfully($user));
      return redirect()->back()->with('message', 'Successfully created a new account. Please check your email and activate your account.');

    }

    public function activateUser(string $activation_code){
      try {
        $user = app(User::class)->where('activation_code', $activation_code)->first();
        if (!$user) {
          return "The code does not exist for any user in our system.";
        }
        $user->status          = 1;
        $user->activation_code = null;
        $user->save();
        auth()->login($user);
      } catch (\Exception $exception) {
        logger()->error($exception);
        return "Whoops! something went wrong.";
      }
      return redirect()->to('/');

    }
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
