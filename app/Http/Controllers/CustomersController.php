<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CustomersController extends Controller{
  public function new(){
    return view('customers.new');
  }

  public function logOut(Request $request){
    Auth::logout();
    return redirect('/');
  }
}
