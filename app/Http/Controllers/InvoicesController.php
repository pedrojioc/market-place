<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Invoice;
use App\InvoiceDetails;
use App\Product;
use App\Helpers\ProductHelper;

class InvoicesController extends Controller{

  public function __construct(){
    parent::__construct();
  }
  public function renderOverView(Request $request){
    return view('order-overview');
  }

  public function store(Request $request){
    if(!Auth::check()){
      return redirect('/login')->with(['after_action' => 'create-order', 'order-data' => $request->data]);
      return back()->with('notice', 'Please login to continue');
    }
    $data = $request->data;
    if($data == NULL){
      return back()->with('notice', 'This data is invalid');
    }
    $discount = ProductHelper::checkDiscount();
    $data = json_decode($data, true);
    
    $subtotal = 0;
    $total = 0;
    $discount_amount = NULL;
    $total_discount = 0;
    $total_shipping = 0;
    $shipping = 0;
    $details_invoice_data = [];
    foreach ($data as $key => $product) {
      $my_product = Product::find($product['id']);
      if($my_product){
        if($discount != NULL){
          $discount_amount = ($my_product->price * $discount);
          $amount = ($my_product->price * $product['quantity']) - $discount_amount;
          $total_discount = $total_discount + $discount_amount;

        }else{
          $amount = ($my_product->price * $product['quantity']);
          $discount_amount = NULL;
        }
        $amount = ($my_product->price * $product['quantity']);
        $subtotal = $subtotal + $amount;
        $shipping = (0.065 * ($my_product->weight * $product['quantity']));
        $details_invoice_data[$key] = [
          "product_id" => $my_product->id,
          "quantity" => $product['quantity'],
          "discount" => $discount_amount,
          "total" => $amount,
          "color" => $product['color']
        ];
        $total_shipping = ($total_shipping + $shipping);
      }
    }//End foreach
    $total_shipping = ProductHelper::round_up($total_shipping, 2);
    $total = $subtotal + $total_shipping;
    $invoice_data = [
      "customer_id" => Auth::user()->id,
      "discount" => $total_discount,
      "subtotal" => $subtotal,
      "shipping" => $total_shipping,
      "total" => $total,
      "payment_method" => 1,
      "status_id" => 3
    ];
    $invoice = new Invoice();
    //return invoice id if saved
    $rs = $invoice->set($request, $invoice_data);
    $rs_details = $this->storeInInvoiceDetails($rs, $details_invoice_data);
    if($rs_details){
      return redirect()->action(
        'ShippingController@new', ['invoice_id' => $rs]
      );
    }else{
      return back()->with('notice', 'This data is invalid');
    }
  }

  public function storeInInvoiceDetails($invoice_id, $data){
    $invoice_details = new InvoiceDetails;
    foreach ($data as $key => $value) {
      $invoice_details = new InvoiceDetails;
      $rs = $invoice_details->set($invoice_id, $value);
    }

    return $rs;
  }


}
