<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DateTime;
use App\Category;
use App\Product;
use App\Package;
use App\HomeSlider;
use App\Helpers\ProductHelper;

class MainController extends Controller{

  public function __construct(){
    parent::__construct();
  }
  public function index(Request $request){
    
    $product = new Product();
    $products = $product->getProducts();
    // $p = Product::find(1);
    // foreach ($p->colors as $key => $color) {
    //   echo $color;
    // }
    // dd($p);
    // $dates = $this->getLastMonths();
    // $last_products = $this->product->getLastProducts($dates);
    $last_products = Product::orderBy('created_at', 'desc')->limit(22)->get();
    $most_viewed_products = Product::orderBy('viewed', 'desc')->limit(22)->get();
    $featured = Product::orderBy('viewed', 'desc')->limit(10)->get();
    // if(Auth::user()->is_member_grunem){
    //   $package = Package::find(Auth::user()->package_id);
    //   $discount = ($package->discount/100);
    //
    // }else{
    //   $discount = NULL;
    // }
    $discount = ProductHelper::checkDiscount();
    $sliders = HomeSlider::where(['status' => 1])->orderBy('order', 'asc')->get();

    return view('index', [
                          'section' => 'homepage',

                          'products' => $products,
                          'last_products' => $last_products,
                          'most_viewed_products' => $most_viewed_products,
                          'featured' => $featured,
                          'discount' => $discount,
                          'sliders' => $sliders
                        ]);
  }

  private function getLastMonths(){
    $date = new DateTime();
    $current_date = $date->format('Y-m-d');

    //Convert date to timestamp and subtract a month
    $start_last_month = strtotime("-1 month", strtotime($current_date));
    $start_last_month = date("Y-m-01", $start_last_month);

    $dates = [$start_last_month, $current_date];
    return $dates;
  }
}
