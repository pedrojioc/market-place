<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use Illuminate\Support\Facades\Storage;
use App\Payment;
use App\Invoice;
use App\Shipping;
use App\Country;
use App\State;


class PaymentsController extends Controller{

  protected $api_btc_price;
  public function __construct(){
    parent::__construct();
    $this->api_btc_price = "https://blockchain.info/tobtc?currency=USD&value=";
  }

  public function new(Request $request){
    $invoice_id = $request->invoice_id;
    $invoice = Invoice::find($invoice_id);
    $increase_by_btc = $invoice->subtotal * 0.15;
    $total = $invoice->total + $increase_by_btc;
    $total = $total - $invoice->discount;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->api_btc_price.$total);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $btc_amount = curl_exec($ch);

    curl_close($ch);

    $qr_code = "https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=bitcoin:14tPwpJU8cofdLd3qU7EFwec3CqK43ZsoV?&amount=".$btc_amount;
    return view('payment', ['invoice' => $invoice, 'btc_amount' => $btc_amount, 'qr_code' => $qr_code])->render();
  }

  public function store(Request $request){
    $payment = new Payment;
    $customer_id = Auth::user()->id;
    if($request->payment_method == "1"){
      $rules = [
        'bank_name' => 'required|string|min:4|max:255',
        'swift_code' => 'required|string|min:4|max:255',
        'amount' => 'required',
        'invoice_id' => 'required',
        'payment_method' => 'required',
        'image'  => 'required|image|mimes:jpeg,jpg,png|max:2048',
      ];
      $validator = Validator::make($request->all(), $rules);
      if ($validator->fails()){
        return back()->with('notice', 'This data is invalid');
      }

      $image = $request->file('image');
      $disk = Storage::disk('s3');
      $image_name = 'payment_'.$request->invoice_id.'.'.$image->getClientOriginalExtension();
      //$new_name = $request->invoice_id . '.' . $image->getClientOriginalExtension();
      $path = 'market-place/payments/'.$customer_id.'/'.$image_name;
      $disk->put($path, fopen($image, 'r+'), 'public');
      $data = [
        "payment_method_id" => $request->payment_method,
        "invoice_id" => $request->invoice_id,
        "bank_name" => $request->bank_name,
        "swift_code" => $request->swift_code,
        "payment_support_image" => $path,
        "bitcoin_transaction_id" => NULL,
        "amount" => $request->amount
      ];

    }else if($request->payment_method == "2"){
      $rules = [
        'amount' => 'required',
        'invoice_id' => 'required',
        'payment_method' => 'required',
        'transaction_id'  => 'required',
      ];
      $validator = Validator::make($request->all(), $rules);
      if ($validator->fails()){
        return back()->with('notice', 'This data is invalid');
      }
      $data = [
        "payment_method_id" => $request->payment_method,
        "invoice_id" => $request->invoice_id,
        "bank_name" => NULL,
        "swift_code" => NULL,
        "payment_support_image" => NULL,
        "bitcoin_transaction_id" => $request->transaction_id,
        "amount" => $request->amount
      ];
    }
    $rs = $payment->set($data);
    if($rs){
      return redirect()->action(
        'PaymentsController@orderComplete', ['invoice_id' => $request->invoice_id]
      );
    }else{
      return back()->with('notice', 'An error has occurred');
    }
  }


  public function orderComplete(Request $request){
    $invoice = Invoice::find($request->invoice_id);
    $shipping = Shipping::where('invoice_id', '=', $request->invoice_id)->firstOrFail();
    $country = Country::find($shipping->country_id);
    $state = State::find($shipping->state_id);
    $invoice->status_id = 1;
    $invoice->save();
    $status = $invoice->invoice_status()->first();
    
    $payment_method = $invoice->payment()->first()->payment_method()->first();

    return view('order-complete', [
                                    'invoice' => $invoice,
                                    'shipping' => $shipping,
                                    'country' => $country,
                                    'state' => $state,
                                    'status' => $status,
                                    'payment_method' => $payment_method,
                                  ]);
  }

  private function updatedInvoice($invoice_id){

  }
}
