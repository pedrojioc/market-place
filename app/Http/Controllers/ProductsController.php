<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Category;
use App\Product;
use App\Color;
use App\Helpers\ProductHelper;
use Illuminate\Support\Facades\DB;

class ProductsController extends Controller{

  public function __construct(){
    parent::__construct();
  }
  public function productsByCategory(Request $request){
    $category = new Category();
    $colors = Color::all();

    $category_id = intval($request->category_id);

    $start = 0;
    $limit = 10;
    if(!empty($request->limit)){
      $start = $request->start;
      $limit = $request->limit;
    }

    $products = Product::where('category_id', '=', $category_id)->where('status', 1);
    // $colors = DB::table('products as ps')
    //                  ->select(DB::raw('ps.color_id, count(color_id) as count_products'))
    //                  ->where('category_id', '=', $category_id)
    //                  ->where('status', '=', 1)
    //                  ->groupBy('color_id')
    //                  ->get();

    if(!empty($request->color) && $request->color != 0){
      $color_filter = $request->color;
      $products = $products->where('color_id', $color_filter);
    }

    if(!empty($request->price_max) && !empty($request->price_min)){
      $min = $request->price_min;
      $max = $request->price_max;
      $products = $products->whereBetween('price', [$min, $max]);
    }
    $total_rows = $products->count();
    //dd($total_rows);

    switch ($request->order) {
      case '1':
        $products = $products->orderBy('name', 'asc');
        break;
      case '2':
        $products = $products->orderBy('name', 'desc');
        break;
      case '3':
        $products = $products->orderBy('price', 'asc');
        break;
      case '4':
        $products = $products->orderBy('price', 'desc');
      default:
        $products = $products->orderBy('name', 'asc');
        break;
    }


    $products = $products->offset($start)->limit($limit);
    $result = $products->get();

    $sections = intval(ceil($total_rows / $limit));
    // if($products->count() == 0){
    //   return redirect('/');
    // }

    $my_category = $category->find($category_id);

    $categories = $category->getCategories();
    $discount = ProductHelper::checkDiscount();
    if($request->ajax()){

      return response()->json([
        'body' => view('contents.products-paginate', ['section' => '',
                                        'category' => $my_category,
                                        'categories' => $categories,
                                        'products' => $result,
                                        'paginate' => $sections,
                                        'shown' => $limit,
                                        'discount' => $discount,
                                        'category_id' => $category_id
                                      ])->render(),
        'paginate' => $sections,
        'shown' => $limit,
        'colors' => $colors
      ]);
    }

    return view('products-category', ['section' => '',
                                      'category' => $my_category,
                                      'categories' => $categories,
                                      'products' => $result,
                                      'paginate' => $sections,
                                      'shown' => $limit,
                                      'discount' => $discount,
                                      'category_id' => $category_id,
                                      'colors' => $colors
                                    ]);
  }

  public function productsFiltered(Request $request){

  }

  public function show(Request $request){
    $product_id = $request->product_id;
    $product = Product::find($product_id);


    if($product == NULL){
      return redirect('/');
    }
    $colors = $product->colors;
    $color = Color::find($product->color_id);
    //$images = $this->getImages($product->id);
    $directory = "market-place/products/".$product->id;
    $images = Storage::disk('s3')->files($directory);
    $discount = ProductHelper::checkDiscount();
    $product->increment('viewed', 01);

    return view('show-product', ['section' => '',
                                 'product' => $product,
                                 'colors' => $colors,
                                 'images' => $images,
                                 'color' => $color,
                                 'discount' => $discount
                              ]);
  }

  public function search(Request $request){
    if($request->terms == NULL){
      return redirect('/');
    }
    $terms = $request->terms;
    $products = Product::where('name', 'like', '%'.$terms.'%')
    ->where('status', 1)
    ->get();
    $discount = ProductHelper::checkDiscount();
    return view('search-products', [
                                    'products' => $products,
                                    'discount' => $discount
                                  ]);
  }

  private function getImages($product_id){
    $files = array();

    $path = public_path().'/images/products/'.$product_id.'/';

    foreach (glob($path."*.png") as $file) {
      $parts = explode("/", $file);
      $files[] = $parts[count($parts)-1];
    }
    return $files;
  }

  private function setViews($product){

  }
}
