<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreShipping;
use Illuminate\Support\Facades\Auth;
use App\Country;
use App\Shipping;
use App\Customer;

class ShippingController extends Controller{
  public function __construct(){
    parent::__construct();
  }
  public function new(Request $request){
    $invoice_id = $request->invoice_id;
    $countries = Country::getCountries();
    return view('shipping', ['countries' => $countries, 'invoice_id' => $invoice_id])->render();
  }

  public function store(Request $request){
    $shipping = new Shipping();
    if($request->registration_address == NULL){
      $validatedData = $request->validate([
        'country_id' => 'required|max:10',
        'state_id' => 'required|max:10',
        'city' => 'required|max:100',
        'address' => 'required|max:100',
        'postal_code' => 'required|max:45',
        'invoice_id' => 'required'
      ]);
      $data = (object) $validatedData;
    } else{
      $data = $this->getCustomer();
      $data->invoice_id = $request->invoice_id;
    }
    $rs = $shipping->set($data);
    if($rs){
      return redirect()->action(
        'PaymentsController@new', ['invoice_id' => $request->invoice_id]
      );
    }else{
      return back();
    }


  }

  private function getCustomer(){
    $customer = Customer::find(Auth::user()->customer_id);

    return $customer;
  }
}
