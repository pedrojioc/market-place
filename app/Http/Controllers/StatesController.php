<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\State;
class StatesController extends Controller{
  protected $country;
  function __construct(){
    $this->state = new State();
  }
  public function getStatesByCountry(Request $request){
    $country = $request->input('country');
    $states = $this->state->getStatesByCountry($country);

    return response()->json($states);
  }
}
