<?php

namespace App\Http\Controllers;

use App\Notifications\UserRegisteredSuccessfully;
use App\User;
use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Country;


class UserApiController extends Controller{
  use RegistersUsers;


  public function registerUserFromGrunem(Request $request){

    $customer = new Customer;
    //dd($request);
    $rules = [
      'email'       => 'required|string|email|max:255|unique:users',
      'first_name'  => 'required|string|max:255',
      'last_name'   => 'required|string|max:255',
      'username'    => 'required|string|max:255|unique:users',
      'country'     => 'required|not_in:0',
      'state'       => 'required|not_in:0',
      'city'        => 'required|string|max:255',
      'address'     => 'required|string|max:255',
      'postal_code' => 'required|string|max:255',
      'password'    => 'required',
      'package_id'     => 'required'
    ];
    $validator = Validator::make($request->all(), $rules);

    if ($validator->fails()){
      return response()->json([
        'status' => '400',
        'message' => 'Unable to create new user. first filter'
      ]);
    }
    $validate_data = [
      'email' => $request->email,
      'first_name' => $request->first_name,
      'last_name' => $request->last_name,
      'username' => $request->username,
      'country' => $request->country,
      'state' => $request->state,
      'city' => $request->city,
      'address' => $request->address,
      'postal_code' => $request->postal_code,
      'password' => $request->password,
      'is_member_grunem' => 1,
      'package_id' => $request->package_id
    ];

    // $validate_data = $request->validate([
    //   'email'       => 'required|string|email|max:255|unique:users',
    //   'first_name'  => 'required|string|max:255',
    //   'last_name'   => 'required|string|max:255',
    //   'username'    => 'required|string|max:255',
    //   'country'     => 'required|not_in:0',
    //   'state'       => 'required|not_in:0',
    //   'city'        => 'required|string|max:255',
    //   'address'     => 'required|string|max:255',
    //   'postal_code' => 'required|string|max:255',
    //   'password'    => 'required|string|min:6|confirmed',
    // ]);

    try {
      $validate_data['password'] = Hash::make(array_get($validate_data, 'password'));
      $validate_data['activation_code'] = str_random(30).time();

      $customer_id = $customer->create($validate_data);
      $validate_data['customer_id'] = $customer_id;
      $validate_data['name'] = $validate_data['first_name']." ".$validate_data['last_name'];

      $user = app(User::class)->create($validate_data);
    } catch (\Exception $exception) {
      logger()->error($exception);
      //return redirect()->back()->with('message', 'Unable to create new user.');
      return response()->json([
        'status' => '400',
        'message' => 'Unable to create new user.'
      ]);
    }
    $user->notify(new UserRegisteredSuccessfully($user));
    //return redirect()->back()->with('message', 'Successfully created a new account. Please check your email and activate your account.');
    return response()->json([
      'status' => '200',
      'message' => 'Successfully created a new account. Please check your email and activate your account.'
    ]);
  }
}
