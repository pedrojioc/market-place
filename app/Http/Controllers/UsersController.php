<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UpdateShipping;
use App\User;
use App\Country;
use App\State;
use App\Customer;
use App\Invoice;
use Carbon\Carbon;

class UsersController extends Controller
{
  public function account(Request $request){
    $user_id = Auth::user()->id;
    $user = User::find($user_id);
    $customer = Customer::find(Auth::user()->customer_id);
    $countries = Country::all();
    $states = State::where('country_id', $customer->country_id)->get();
    $customer->country = $customer->country()->first();
    $customer->state = $customer->state()->first();
    $orders = $this->getOrderList($user_id);
    return view('account', [
                            'user' => $user,
                            'countries' => $countries,
                            'states' => $states,
                            'customer' => $customer,
                            'orders' => $orders
                          ]);
  }

  public function updateShippingAddress(UpdateShipping $request){
    $json_data = array(
      "status" => 422
    );
    $customer = Customer::find(Auth::user()->customer_id);
    $customer->country_id = $request->country;
    $customer->state_id = $request->state;
    $customer->city = $request->city;
    $customer->address = $request->address;
    $customer->postal_code = $request->postal_code;
    if($customer->save()){
      $json_data["status"] = 200;
    }
    echo json_encode($json_data);
  }

  private function getOrderList($user_id){
    $orders = Invoice::where('customer_id', $user_id)->where('status_id', '<>', 3)->limit(10)->get();
    foreach ($orders as $key => $order) {
      $order->details = $order->invoice_details()->get();
      $date = Carbon::parse($order->created_at, 'UTC');
      $order->date = $date->format('jS \\of F Y');
      $order->status = $order->invoice_status()->first();
      foreach ($order->details as $keyd => $detail) {
        $detail->product = $detail->product()->first();
      }
    }
  
    return $orders;
  }
}
