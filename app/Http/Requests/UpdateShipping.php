<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateShipping extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'country' => 'required|not_in:0',
          'state' => 'required|not_in:0',
          'city' => 'required|max:255',
          'address' => 'required',
          'postal_code' => 'required|max:255',
        ];
    }
}
