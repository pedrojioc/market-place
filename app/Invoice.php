<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model{
  public function invoice_details(){
    return $this->hasMany('App\InvoiceDetails');
  }
  public function invoice_status(){
    return $this->belongsTo('App\InvoiceStatus', 'status_id');
  }
  public function payment(){
    return $this->hasOne('App\Payment');
  }
  // public function payment_method(){
  //   return $this->belongsTo('App\PaymentMethod', 'payment_method_id');
  // }
  public function shipping(){
    return $this->hasOne('App\Shipping');
  }

  public function set($request, $data){
    $this->customer_id = $data["customer_id"];
    $this->discount = $data["discount"];
    $this->subtotal = $data["subtotal"];
    $this->shipping = $data["shipping"];
    $this->total = $data["total"];
    $this->payment_method_id = $data["payment_method"];
    $this->status_id = $data["status_id"];
    $rs = $this->save();

    return $this->id;
  }
}
