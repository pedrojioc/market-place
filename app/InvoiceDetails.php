<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceDetails extends Model{
  protected $table = "invoice_details";

  public function invoice(){
    return $this->belongsTo("App\Invoice");
  }
  public function product(){
    return $this->belongsTo("App\Product");
  }

  public function set($invoice_id, $data){
    $this->invoice_id = $invoice_id;
    $this->product_id = $data['product_id'];
    $this->color_id = $data['color'];
    $this->quantity = $data['quantity'];
    $this->discount = $data['discount'];
    $this->total = $data['total'];
    $rs = $this->save();

    return $rs;
  }
}
