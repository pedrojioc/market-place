<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceStatus extends Model
{
  protected $table = 'invoices_status';
  public function invoice(){
    return $this->hasMany('App\Invoice');
  }
}
