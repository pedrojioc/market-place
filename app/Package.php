<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model{
  protected $primaryKey = "package_id";
  public function users(){
    return $this->hasMany("App\User");
  }
}
