<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model{

  public function invoice(){
    return $this->belongsTo('App\Invoice', 'invoice_id');
  }
  public function payment_method(){
    return $this->belongsTo('App\PaymentMethod', 'payment_method_id');
  }
  public function set($data){
    $this->payment_method_id = $data["payment_method_id"];
    $this->invoice_id = $data["invoice_id"];
    $this->bank_name = $data["bank_name"];
    $this->swift_code = $data["swift_code"];
    $this->payment_support_image = $data["payment_support_image"];
    $this->bitcoin_transaction_id = $data["bitcoin_transaction_id"];
    $this->amount = $data["amount"];

    $rs = $this->save();
    return $rs;
  }
}
