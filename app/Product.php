<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Product extends Model{
  protected $table = 'products';

  // public function colors(){
  //   return $this->belongsToMany('App\Color')->using('App\ProductColor')->withPivot('stock', 'viewed');
  // }
  public function colors(){
    return $this->belongsTo('App\Color');
  }
  public function category(){
    return $this->belongsTo('App\Category');
  }
  public function invoice_details(){
    return $this->hasMany('App\InvoiceDetails');
  }


  public function getProducts(){
    $products = DB::table($this->table)->select('*')->get();

    return $products;
  }

  public function getLastProducts($dates){
    $query = "SELECT * FROM products as ps WHERE DATE_FORMAT(ps.created_at, '%Y-%m-%d') BETWEEN '$dates[0]' AND '$dates[1]'";
    $products = DB::select($query);
    $products = json_decode(json_encode($products), true);

    return $products;
  }

  public function getFeaturedProducts(){}

  public function getSpecialProducts(){
    $products = DB::table($this->table)->select('*')->get();

    return $products;
  }
}
