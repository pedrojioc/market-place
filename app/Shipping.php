<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model{
  protected $table = 'shipping';

  public function invoice(){
    return $this->belongsTo('App\Invoice');
  }
  public function country(){
    return $this->hasOne('App\Country');
  }
  public function state(){
    return $this->hasOne('App\State');
  }
  public function set($request){
    $this->country_id = $request->country_id;
    $this->state_id = $request->state_id;
    $this->city = $request->city;
    $this->address = $request->address;
    $this->postal_code = $request->postal_code;
    $this->invoice_id = $request->invoice_id;
    $result = $this->save();
    return $result;
  }
}
