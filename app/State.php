<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class State extends Model{
  protected $table = 'states';

  public function shipping(){
    return $this->belongsTo('App\Shipping');
  }

  public function customer(){
    return $this->hasMany('App\Customer');
  }

  public static function getStatesByCountry($country_id){
    $states = DB::table('states')->select('id', 'name')->where('country_id', $country_id)->get();

    return $states;
  }
}
