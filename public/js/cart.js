// $ function is loaded only once HTML has completely loaded
//


	var done = false;

    function isCartEmpty() {
      var counter = 0;
			var key = '';
      for(let i = 0; i<=localStorage.length; i++)
      {
				key = localStorage.key(i);
        if (key != null && key.substring(0,5) == 'prod_') {
          counter++;
        }
      }
      return counter;
    }

    function setTotalCost() {
    	var total_cost = 0;
			var key = '';
			for(let i = 0; i<=localStorage.length; i++){
				key = localStorage.key(i);
				if(key != null && key.substring(0,5) == 'prod_') {
					var qty = JSON.parse(localStorage.getItem(key)).quantity;
					var cost = JSON.parse(localStorage.getItem(key)).price;
					total_cost += qty*cost;
				}
			}

    	if (localStorage.getItem('total_cost')) {
		    $('#_subtotal').text(total_cost);
		    localStorage.setItem('total_cost', total_cost);
			}
			else {
		   	localStorage.setItem('total_cost', '0');
		   	$('#_subtotal').text('0');
			}
    } // end of the function setTotalCost

    function setNoOfProducts() {
    	var no_of_products = 0;
			var key = '';
    	for(let i = 0; i<=localStorage.length; i++){
				key = localStorage.key(i);
				if(key != null && key.substring(0,5) == 'prod_') {
					no_of_products += JSON.parse(localStorage.getItem(key)).quantity;
				}
    	}
    	localStorage.setItem('no_of_products', no_of_products);
    	if (localStorage.getItem('no_of_products')) {
    		no_of_products = +localStorage.getItem('no_of_products');
    		$('#num_of_prods').text(no_of_products);
    	}
    	else {
    		localStorage.setItem('no_of_products', '0');
    		no_of_products = +localStorage.getItem('no_of_products');
    		$('#num_of_prods').text(no_of_products);
    	}
    } // end of the function setNoOfProucts

    function updateCart() {

    	no_of_products = +localStorage.getItem('no_of_products');
    	if (no_of_products) {
    		// if (!done) {
    		// 	var cart_head = $('#cartItemsHead');
    		// 	var head_string = "<tr><th>Product Name</th><th>Quantity</th><th>Amount</th><tr>";
    		// 	cart_head.append(head_string);
    		// 	done = true;
    		// }

    		// display using cart.append
    		var cart_body = $('#cart_items_header');
    		cart_body.empty(); // to delete its elements
				var key = '';
    		for(let i = 0; i<=localStorage.length; i++){
					key = localStorage.key(i);
    			if(key != null && key.substring(0,5) == 'prod_') {
    				cartItem = JSON.parse(localStorage.getItem(key));
    				var name = cartItem.name;
    				var price = +cartItem.price;
    				var amount = price*cartItem.quantity;
    				var delCart = "delete_cart_item";
    				var cartString = '<li> <a class="close-cart" data-id="'+cartItem.id+'" name="delete_cart_item"><i class="fa fa-times-circle"></i></a>'+
																'<div class="media"> <a class="pull-left"> <img alt="" src="'+cartItem.image+'"></a>'+
																	'<div class="media-body"> <span><a>'+cartItem.name+'</a></span>'+
																		'<p class="cart-price">$'+cartItem.price+'</p>'+
																		'<div class="product-qty">'+
																			'<label>Qty:</label>'+
																			'<div class="custom-qty">'+
																				'<input type="text" name="qty" maxlength="8" value="'+cartItem.quantity+'" title="Qty" class="input-text qty">'+
																			'</div>'+
																		'</div>'+
																	'</div>'+
																'</div>'+
															'</li>';
    				cart_body.append(cartString);
    			}
    		}
        setCartStyle(); // call of the function setCartStyle
    	}
    	else
    	{
    		var cart_head = $('#cartItemsHead');
    		cart_head.empty(); // to remove the child elements
    		var cart_body = $('#cart_items_header');
    		cart_body.empty(); // to delete its elements
    	}

    } // end of the function updateCart

    function setCartStyle() { // Using this function to set style of cminus and cplus buttons
        var cButtonsPlus = $('button[name="cplus"]');
        var cButtonsMinus = $('button[name="cminus"]');
        for (var i = 0; i < 3 ; i++) {
            if (cButtonsMinus[i]) {
            cButtonsMinus[i].style.float = "left";
            cButtonsPlus[i].style.float = "right";
            }
        }
    }

    function cartRefresh() { // every time the page is loaded, the card is refreshed

    	setTotalCost(); // call of the function setTotalCost
    	setNoOfProducts(); // call of the function setNoOfProducts
    	updateCart(); // call of the function updateCart
    } // end of the function cartRefresh

    function init() {
        // setTable
    	for (var i = 1; i <= 3 ; i++) {
    		$('quantity[id=' + i +']').text(1);
    	}
    	cartRefresh(); // call of the function cartRefresh

    } // end of the function init

    init(); // call of the function init

    function reset() {
    	localStorage.setItem('no_of_products', 0);
    	no_of_products = +localStorage.getItem('no_of_products');
    	$('#totalCost').text(no_of_products);
    	localStorage.setItem('total_cost', 0);
    	total_cost = +localStorage.getItem('total_cost');
    	$('#num_of_prods').text(total_cost);
    	for (var i = 1; i <= 3 ; i++) {
    		$('quantity[id=' + i +']').text(1);
    	}
    	var cart_head = $('#cartItemsHead');
    	cart_head.empty(); // to remove the child elements
    	var cart_body = $('#cartItemsBody');
    	cart_body.empty(); // to remove the child elements
    	done = false;
    } // end of the function reset

    function qtyDecrement(qty_id) {
    	if (($('quantity[id=' + qty_id + ']').text())>1) { // Quantity can't be lesser than 1
    		var x = +$('quantity[id=' + qty_id + ']').text();
    		$('quantity[id=' + qty_id + ']').text(--x);
    	}
    } // end of the function qtyDecrement

    function qtyIncrement(qty_id) {
    	var x = +$('quantity[id=' + qty_id + ']').text();
    	$('quantity[id=' + qty_id + ']').text(++x);
    } // end of the function qtyIncrement

    // Very important function
    $('body').on('click', '.close-cart' , function() { // To delete elements after they've been dynamically updated
    	if (this.name == "delete_cart_item") {

 				localStorage.removeItem('prod_' + $(this).data('id'));
 				cartRefresh();
 			 	if(!isCartEmpty()){
 					done = false;
 			 	}
    	}

        if (this.name == "cminus") {
            $('cquant[id=' + this.id + ']').text()
            if (($('cquant[id=' + this.id + ']').text())>1) { // Quantity can't be lesser than 1
            var x = +$('cquant[id=' + this.id + ']').text();
            $('cquant[id=' + this.id + ']').text(--x);
            }
            var qty = +$('cquant[id=' + this.id + ']').text();
            cartItem = JSON.parse(localStorage.getItem('prod_'+this.id));
                newcartItem = {
                    'id': this.id,
                    'quantity': (qty)
                };
                localStorage.removeItem('prod_'+this.id)
                localStorage.setItem('prod_' + this.id, JSON.stringify(newcartItem));
            cartRefresh();
        }

     });


    $('body').on('click', '.green' , function() { // To delete elements after they've been dynamically updated
        if (this.name == "cplus") {
            var x = +$('cquant[id=' + this.id + ']').text();
            $('cquant[id=' + this.id + ']').text(++x);
        var qty = +$('cquant[id=' + this.id + ']').text();
        cartItem = JSON.parse(localStorage.getItem('prod_'+this.id));
                newcartItem = {
                    'id': this.id,
                    'quantity': (qty)
                };
                localStorage.removeItem('prod_'+this.id)
                localStorage.setItem('prod_' + this.id, JSON.stringify(newcartItem));
        				cartRefresh();
        }
     });



		 $('.add-to-cart').click(function (){
			 var qty = parseInt($('#prod_quantity_'+this.id).val());
			 var price = $('#prod_price_'+this.id).val();
			 var name = $('#prod_name_'+this.id).val();
			 var color = $('#prod_color_'+this.id).val();
			 var weight = $('#prod_weight_'+this.id).val();
			 // add to cart (local)
			 if (localStorage.getItem('prod_'+this.id)) {
				 cartItem = JSON.parse(localStorage.getItem('prod_'+this.id));
				 newcartItem = {
					 'id': this.id,
					 'name': name,
					 'quantity': (qty+parseInt(cartItem.quantity)),
					 'price': price,
					 'image': $('#prod_image_'+this.id).val(),
					 'color': color
				 };

				 localStorage.removeItem('prod_'+this.id)
				 localStorage.setItem('prod_' + this.id, JSON.stringify(newcartItem));
			 }else {
				 newcartItem = {
					 'id': this.id,
					 'name': name,
					 'quantity': qty,
					 'price': price,
					 'image': $('#prod_image_'+this.id).val(),
					 'color': color,
					 'weight': weight
				 };

				 localStorage.setItem('prod_' + this.id, JSON.stringify(newcartItem));
			 }

				cartRefresh(); // call of the function cartRefresh
				setCartStyle(); // call of the function setCartStyle
				$('quantity[id=' + this.id + ']').text(1);
				// Display a success toast
				toastr.options = {
				  "closeButton": true,
				  "debug": false,
				  "newestOnTop": false,
				  "progressBar": false,
				  "positionClass": "toast-top-center",
				  "preventDuplicates": false,
				  "onclick": null,
				  "showDuration": "300",
				  "hideDuration": "1000",
				  "timeOut": "1500",
				  "extendedTimeOut": "1000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "fadeIn",
				  "hideMethod": "fadeOut"
				}
				toastr.success('Product added to your cart');
		 });


    // $("button").click(function() { // button click function
		//
    // 	if (this.name == "plus") { // if '-' button is clicked
    // 		qtyIncrement(this.id); // increase quantity by 1
    // 	}
		//
    // 	if (this.name == "minus") { // if '-' button is clicked
    // 		qtyDecrement(this.id); // decrease quantity by 1
    // 	}
		//
		//
		//
    // 	if (this.name == "checkout") {
    //         alert('Thank you for shopping!');
    // 		reset(); // call of the function reset
    // 		for (var i = 3; i >= 1; i--) {
    // 			localStorage.removeItem('prod_' + i);
    // 		}
    // 	}
		//
		// });
