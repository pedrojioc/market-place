//Wishlist

async function requestWishlist( data ){
  let result;
  try {
    result = await $.ajax({
      type: 'POST',
      url: data.url,
      data: {
        '_token': $('input[name=_token]').val(),
        'product_id': data.productId
      }
    })
  } catch (e) {
    //Debug true only in development
    console.log('Error:');
    console.log('Status: '+e.statusText);
  }

  return result;
}

$(document).ready(function() {

  $(document).on('click', '.wishlist', function(e) {
    e.preventDefault();
    console.log('Wishlist');
    $(this).parent().toggleClass('active');
    let option = $(this).parent().hasClass('active')
    let urlRequest = (option) ? '/user/wishlist/create' : '/user/wishlist/destroy';
    let configRequest = {
      'url': urlRequest,
      'productId': $(this).data('product-id')
    }

    console.log(configRequest)

  })
});
