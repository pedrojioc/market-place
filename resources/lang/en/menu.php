<?php
  return [
    "top_links" => [
      0 => "Your Grunem.com",
      1 => "Today's Deals",
      2 => "Help",
      3 => "<strong class='ml-2'>Easter Deals</strong> up to 50%"
    ],

    "search" => [
      0 => "All Categories",
      1 => "Search entire store here..."
    ],
    "dropdown_user" => [
      0 => "My Account",
      1 => "Log Out"
    ],
    "dropdown_cart" => [
      0 => "Cart Subtotal"
    ],
    "auth" => [
      0 => "Login",
      1 => "Signup"
    ],


  ];
