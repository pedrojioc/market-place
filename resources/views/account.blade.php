@extends('layouts.application')
@section('content')
  <!-- Sidebar Mobile -->
  @include('contents.sidebar-menu-mobile')
  <!-- ./Sidebar Mobile -->

  <!-- CONTAIN START -->
  <section class="checkout-section ptb-95">
    <div class="container">
      <div class="row">
        <div class="col-md-3 col-sm-4">
          <div class="account-sidebar account-tab mb-xs-30">
            <div class="dark-bg tab-title-bg">
              <div class="heading-part">
                <div class="sub-title"><span></span> My Account</div>
              </div>
            </div>
            <div class="account-tab-inner">
              <ul class="account-tab-stap">
                <li id="step1" class="active"> <a href="javascript:void(0)">My Dashboard<i class="fa fa-angle-right"></i> </a> </li>
                <li id="step2"> <a href="javascript:void(0)" class="relative">Account Details<i class="fa fa-angle-right"></i> @if(!UserHelper::profileComplete())<span><small id="" class="notification a-details-notification"></small></span>@endif</a> </li>
                <li id="step3"> <a href="javascript:void(0)">My Order List<i class="fa fa-angle-right"></i> </a> </li>
                {{-- <li id="step4"> <a href="javascript:void(0)">Change Password<i class="fa fa-angle-right"></i> </a> </li> --}}
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-9 col-sm-8">
          <div id="data-step1" class="account-content" data-temp="tabdata">
            <div class="row">
              <div class="col-xs-12">
                <div class="heading-part heading-bg mb-30">
                  <h2 class="heading m-0">Account Dashboard</h2>
                </div>
              </div>
            </div>
            <div class="mb-30">
              <div class="row">
                <div class="col-xs-12">
                  <div class="heading-part">
                    <h3 class="sub-heading">Hello, {{ $user->name }}</h3>
                  </div>
                  {{-- <p>Subscribe to the Print Doodles mailing list to receive updates on new product, special offers and other discount information.<a class="account-link" id="subscribelink" href="#">Click Here</a></p> --}}
                </div>
              </div>
            </div>
            <div class="m-0">
              <div class="row">
                <div class="col-xs-12 mb-20">
                  <div class="heading-part">
                    {{-- <h3 class="sub-heading">Account Information</h3> --}}
                  </div>
                  <hr>
                </div>
                <div class="col-md-6 col-sm-12">
                  <div class="cart-total-table address-box commun-table">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Shipping Address</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><ul>
                                <li class="inner-heading"> <b>{{ $customer->country->name }}</b> </li>
                                <li>
                                  <p>{{ $customer->address }}</p>
                                </li>
                                <li>
                                  <p>{{ $customer->city }}, {{ $customer->state->name }}</p>
                                </li>
                                <li>
                                  <p>{{ $customer->postal_code }}</p>
                                </li>
                              </ul></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                {{-- <div class="col-sm-6">
                  <div class="cart-total-table address-box commun-table">
                    <div class="table-responsive">
                      <table class="table">
                        <thead>
                          <tr>
                            <th>Billing Address</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><ul>
                                <li class="inner-heading"> <b>Denial tom</b> </li>
                                <li>
                                  <p>5-A kadambari aprtment,opp. vasan eye care,</p>
                                </li>
                                <li>
                                  <p>Risalabaar,City Road, deesa-405001.</p>
                                </li>
                                <li>
                                  <p>India</p>
                                </li>
                              </ul></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div> --}}
              </div>
            </div>
          </div>
          <div id="data-step2" class="account-content" data-temp="tabdata" style="display:none">
            <div class="row">
              <div class="col-xs-12">
                <div class="heading-part heading-bg mb-30">
                  <h2 class="heading m-0">Account Details</h2>
                </div>
              </div>
            </div>
            <div class="m-0">
              <form class="main-form full">
                <div class="mb-20">
                  <div class="row">
                    <div class="col-xs-12 mb-20">
                      <div class="heading-part">
                        <h3 class="sub-heading">Shipping Address</h3>
                        @if(!UserHelper::profileComplete())
                        <p class="text-info">Please complete the information shipping</p>
                        @endif
                      </div>
                      <hr>
                    </div>
                    <div class="col-sm-6">
                      <div class="input-box">
                        <input type="text" required placeholder="Full Name" value="{{ $user->name }}" disabled>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="input-box">
                        <input type="email" required placeholder="Email Address" value="{{ $user->email }}" disabled>
                      </div>
                    </div>
                    {{-- <div class="col-sm-6">
                      <div class="input-box">
                        <input type="text" required placeholder="Company">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="input-box">
                        <input type="text" required placeholder="Contact Number">
                      </div>
                    </div> --}}


                    <div class="col-sm-6">
                      <div class="input-box">
                        <select name="country" id="country" required>
                          <option value="00">Please select your country</option>
                          @foreach ($countries as $key => $country)
                            @if ($country->id == $customer->country_id)
                              <option value="{{$country->id}}" selected>{{ $country->name }}</option>
                            @else
                              <option value="{{$country->id}}">{{ $country->name }}</option>
                            @endif
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="input-box">
                        <select name="state" id="state" required>
                          @if ($customer->state_id == 0 || $customer->state_id == NULL)
                            <option value="00" selected>Please select your state</option>
                          @else
                            <option value="00">Please select your state</option>
                          @endif
                          @foreach ($states as $key => $state)
                            @if($state->id == $customer->state_id)
                              <option value="{{$state->id}}" selected>{{ $state->name }}</option>
                            @else
                              <option value="{{$state->id}}">{{ $state->name }}</option>
                            @endif
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="input-box">
                        <input type="text" name="city" id="city" required placeholder="Select City" value="{{ $customer->city }}">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="input-box">
                        <input type="text" name="postal_code" id="postal_code" required placeholder="Postcode/zip" value="{{$customer->postal_code}}">
                      </div>
                    </div>
                    <!-- -->
                    <div class="col-sm-12">
                      <div class="input-box">
                        <input type="text" name="address" id="address" required placeholder="Example: stree 118 #90 - 56 " value="{{ $customer->address }}">
                        <span>Please provide the number and street.</span> </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <button type="button" id="save_info_shipping" class="btn btn-color right-side">Save</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <!-- Order list -->
          <div id="data-step3" class="account-content" data-temp="tabdata" style="display:none">
            <div class="row">
              <div class="col-xs-12">
                <div class="heading-part heading-bg mb-30">
                  <h2 class="heading m-0">My Orders</h2>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 mb-xs-30">
                @foreach ($orders as $key => $order)
                  <div class="cart-item-table commun-table">
                    <div class="table-responsive">
                      <div class="group-tables">
                        {{-- <table id="thead-{{$key}}" class="table" data-toggle="collapse" data-target="#tbody-{{$key}}">
                        </table> --}}
                        <table  class="table">
                          <thead id="thead-{{$key}}" data-toggle="collapse" data-target="#tbody-{{$key}}">
                            <tr>
                              <th colspan="4">
                                <ul>
                                  <li><span>Order placed</span> <span>{{ $order->date }}</span></li>
                                  <li class="price-box"><span>Total</span> <span class="price">${{ $order->total - $order->discount }}</span></li>
                                  <li><span>Status</span> <span>{{ $order->status->name }}</span></li>
                                  <li><span>Order No.</span> <span><a href="{{url('/order/order-complete/'.$order->id)}}">#{{$order->id}}</a></span></li>
                                </ul>
                              </th>
                            </tr>
                          </thead>
                          <tbody id="tbody-{{$key}}" class="collapse">
                            @foreach ($order->details as $key_d => $detail)
                              <tr>
                                <td>
                                  <a href="">
                                    <div class="product-image"><img alt="" src="http://grunem-marketplace.s3.amazonaws.com/market-place/products/{{ $detail->product->id ."/". $detail->product->cover_image }}"></div>
                                  </a>
                                </td>
                                <td>
                                  <div class="product-title"> <a href="{{url('product/')}}/{{ $detail->product->id }}">{{ $detail->product->name }}</a> </div>
                                  <div class="product-info-stock-sku m-0">
                                    <div>
                                      <label>Quantity: </label>
                                      <span class="info-deta">1</span>
                                    </div>
                                  </div>
                                </td>
                                <td><div class="base-price price-box"> <span class="price">${{ $detail->total - $detail->discount }}</span> </div></td>

                              </tr>
                            @endforeach
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <br>
                @endforeach
              </div>
            </div>
          </div>
          <!-- End order list -->
          {{-- <div id="data-step4" class="account-content" data-temp="tabdata" style="display:none">
            <div class="row">
              <div class="col-xs-12">
                <div class="heading-part heading-bg mb-30">
                  <h2 class="heading m-0">Change Password</h2>
                </div>
              </div>
            </div>
            <form class="main-form full">
              <div class="row">
                <div class="col-xs-12">
                  <div class="input-box">
                    <label for="old-pass">Old-Password</label>
                    <input type="password" placeholder="Old Password" required id="old-pass">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="input-box">
                    <label for="login-pass">Password</label>
                    <input type="password" placeholder="Enter your Password" required id="login-pass">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="input-box">
                    <label for="re-enter-pass">Re-enter Password</label>
                    <input type="password" placeholder="Re-enter your Password" required id="re-enter-pass">
                  </div>
                </div>
                <div class="col-xs-12">
                  <button class="btn-color" type="submit" name="submit">Change Password</button>
                </div>
              </div>
            </form>
          </div> --}}
        </div>
      </div>
    </div>
  </section>
  <!-- CONTAINER END -->

@endsection

@section('customcss')
  <link rel="stylesheet" href="{{ asset('css/toast.css') }}">
  <style media="screen">
  .notification{
    background: #ec7e00;
    color: #333333;
    border-radius: 50%;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    -o-border-radius: 50%;
    height: 10px;
    line-height: 15px;
    text-align: center;
    width: 10px;
  }
  .a-details-notification{
    position: absolute;
    top: 15px;
    right: 12px;
  }
  </style>
@endsection

@section('custom-js')
  <script type="text/javascript">

    $( document ).ready(function() {
      /*Get states by country*/
      $("#country").change(function (){
        $('#state').children('option:not(:first)').remove();
        let country = $(this).val();
        $.ajax({
          type: 'POST',
          url: '/states',
          dataType: 'json',
          data: {
            'country': country
          },
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},

        }).done(function(data) {
            //console.log(data);
            data.forEach(function(state){
              $('#state').append($("<option></option>").attr("value", state.id).text(state.name));
            });
          })
      });

      $('#save_info_shipping').click(function (){
        let country = document.getElementById('country').value;
        let state = document.getElementById('state').value;
        let city = document.getElementById('city').value;
        let postal_code = document.getElementById('postal_code').value;
        let address = document.getElementById('address').value;
        if(country == '00' || state == '00' || city == '' || postal_code == '' || address == ''){
          toastr.options.progressBar = false;
          toastr.error('Complete all fields');
          return false;
        }
        $.ajax({
          type: 'POST',
          url: '/user/account/update-address',
          dataType: 'json',
          data: {
            'country': country,
            'state': state,
            'city': city,
            'postal_code': postal_code,
            'address': address
          },
          headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        }).done(function(result){
          toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          }
          if(result.status == 200){
            toastr.info('Your shipping address has been updated');
          } else{
            toastr.error('Your address could not be updated');
          }
        }).fail(function(jqXHR, textStatus, errorThrown){
          console.log('error');
          console.log(errorThrown);
        });
      });

    });
  </script>
@endsection
