@extends('layouts.landing')

@section('content')
  <form method="POST" action="{{ route('login') }}">
      @csrf
      <input type="hidden" name="after_action" value="{{ session('after_action') }}">
      <input type="hidden" name="data" value="{{ session('order-data') }}">
    @if(session()->has('login_error'))
      <div class="alert alert-danger">
        {{ session()->get('login_error') }}
      </div>
    @endif
    <div class="row form-group">
      <div class="col-sm-12">
        <input id="identity" type="identity" class="form-control{{ $errors->has('identity') ? ' is-invalid' : '' }}" name="identity" value="{{ old('identity') }}" placeholder="Enter your email or username" required autofocus>
        @if ($errors->has('identity'))
          <span class="invalid-feedback">
            <strong>{{ $errors->first('identity') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="row form-group">
      <div class="col-sm-12">
        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
        @if ($errors->has('password'))
          <span class="invalid-feedback">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">

        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
          <label class="custom-control-label" for="remember">{{ __('Remember Me') }}</label>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-sm-12 text-right">
        <button type="submit" class="btn btn-outline-primary">
          {{ __('Login') }}
        </button>
      </div>
    </div>
    <div class="row justify-content-end">

      {{-- <a class="btn btn-link" href="{{ route('password.request') }}">
        {{ __('Forgot Your Password?') }}
      </a> --}}

    </div>
  </form>
@endsection

@section('custom-css')
  <style media="screen">
    .alert-danger {
      color: #ff152b;
      background-color: #ffffff;
      border-color: #ff0076;
    }
  </style>
@endsection
