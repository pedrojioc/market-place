{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}

@extends('layouts.landing')

@section('content')
  <div class="card-body">
    @if(session()->has('message'))
      <div class="alert alert-success">
        {{ session()->get('message') }}
      </div>
    @endif
    <form method="POST" action="{{ route('register') }}">
      @csrf
      <input type="hidden" name="sponsor" value="{{ $sponsor }}">
      <div class="form-row">


        <div class="form-group col-md-12">
          <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">

            @if ($errors->has('email'))
              <span class="invalid-feedback">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif

        </div>

        <div class="form-group col-md-6">

          <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required placeholder="First Name">

          @if ($errors->has('first_name'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('first_name') }}</strong>
            </span>
          @endif

        </div>

        <div class="form-group col-md-6">

          <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required placeholder="Last Name">

          @if ($errors->has('last_name'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('last_name') }}</strong>
            </span>
          @endif

        </div>

        <!-- Country -->
        <div class="form-group col-md-12">

          <select class="form-control {{ $errors->has('country') ? ' is-invalid' : '0' }}" id="country" name="country" required>
            <option value="0">Country</option>
            @foreach ($countries as $key => $country)
              <option value="{{ $country->id }}">{{ $country->name }}</option>
            @endforeach
          </select>
          @if ($errors->has('country'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('country') }}</strong>
            </span>
          @endif

        </div>

        <!-- State -->
        <div class="form-group col-md-12">

          <select class="form-control {{ $errors->has('state') ? ' is-invalid' : '0' }}" id="state" name="state" required>
            <option value="0">State</option>
          </select>
          @if ($errors->has('state'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('state') }}</strong>
            </span>
          @endif

        </div>


        <div class="form-group col-md-8">

          <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}" required placeholder="City">

          @if ($errors->has('city'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('city') }}</strong>
            </span>
          @endif

        </div>

        <div class="form-group col-md-4">

          <input id="postal_code" type="text" class="form-control{{ $errors->has('postal_code') ? ' is-invalid' : '' }}" name="postal_code" value="{{ old('postal_code') }}" required placeholder="Postal Code">

          @if ($errors->has('postal_code'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('postal_code') }}</strong>
            </span>
          @endif

        </div>

        <div class="form-group col-md-12">

          <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required placeholder="Address">

          @if ($errors->has('address'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('address') }}</strong>
            </span>
          @endif

        </div>

        <div class="form-group col-md-12">
          <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
          @if ($errors->has('password'))
            <span class="invalid-feedback">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group col-md-12">

            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required placeholder="Confirm Password">

        </div>

        <div class="form-group col-md-12 offset-md-4 mb-0">
            <button type="submit" class="btn btn-primary">
              {{ __('Register') }}
            </button>
        </div>
      </div>
    </form>
  </div>
@endsection

@section('custom-js')
  <script type="text/javascript">
    $("#country").change(function (){
      $('#state').children('option:not(:first)').remove();
      let country = $(this).val();
      $.ajax({
        type: 'POST',
        url: '/states',
        data: {
          '_token': $('input[name=_token]').val(),
          'country': country
        },

        success: function(data) {
          //console.log(data);
          data.forEach(function(state){
            $('#state').append($("<option></option>").attr("value", state.id).text(state.name));
          });
        }
      });
    });
  </script>
@endsection
