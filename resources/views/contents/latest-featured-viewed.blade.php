<!--  Featured Products Slider Block Start  -->
<div class="featured-product mt-60">
  <div class="">
    <div class="product-listing">
      <div class="row">
        <div class="col-xs-12">
          <div class="heading-part mb-30">
            <div id="tabs" class="category-bar">
              <ul class="tab-stap">
                <li><a class="tab-step1 selected" title="step1">latest</a></li>
                <li><a class="tab-step2" title="step2">Featured</a></li>
                <li><a class="tab-step3" title="step3">Most Viewed</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div id="items">
          <div class="tab_content pro_cat">
            <ul>
              <!-- Lasat Products -->
              <li>
                <div id="data-step1" class="items-step1 product-slider-main position-r selected" data-temp="tabdata">
                  <div class="row">
                    <div class="tab_cat">
                      <div class="owl-carousel tab_slider">
                        @foreach ($last_products as $key => $product)
                        <div class="item">
                          @include('contents.product')
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <!-- End Last products -->
              <li>
                <div id="data-step2" class="items-step2 product-slider-main position-r" data-temp="tabdata">
                  <div class="row">
                    <div class="tab_cat">
                      <div class="owl-carousel tab_slider">
                        @foreach ($last_products as $key => $product)
                        <div class="item">
                          @include('contents.product')
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <li>
                <div id="data-step3" class="items-step3 product-slider-main position-r" data-temp="tabdata">
                  <div class="row">
                    <div class="tab_cat">
                      <div class="owl-carousel tab_slider">
                        @foreach ($most_viewed_products as $key => $product)
                          <div class="item">
                            @include('contents.product')
                          </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  Featured Products Slider Block End  -->
