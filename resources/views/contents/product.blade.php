
<div class="product-item">
  <input type="hidden" id="prod_name_{{$product->id}}" name="" value="{{ $product->name }}">
  @if($discount != NULL)
    <input type="hidden" id="prod_price_{{$product->id}}" name="" value="{{ $product->price - ($product->price * $discount) }}">
  @else
    <input type="hidden" id="prod_price_{{$product->id}}" name="" value="{{ $product->price }}">
  @endif
  <input type="hidden" id="prod_quantity_{{$product->id}}" name="" value="1">
  <input type="hidden" id="prod_color_{{$product->id}}" name="" value="{{ $product->color_id }}">
  <input type="hidden" id="prod_weight_{{$product->id}}" name="" value="{{ $product->weight }}">
  <input type="hidden" id="prod_image_{{$product->id}}" name="" value="http://grunem-marketplace.s3.amazonaws.com/market-place/products/{{ $product->id ."/". $product->cover_image }}">
  <div class="product-image"> <a href="{{ url('product/'.$product->id) }}"> <img src="http://grunem-marketplace.s3.amazonaws.com/market-place/products/{{ $product->id ."/". $product->cover_image }}" alt="{{ $product->name }}"> </a>
    <div class="product-detail-inner">
      <div class="detail-inner-left left-side">
        <ul>
          <li class="pro-cart-icon">

            <button id="{{$product->id}}" name="add-to-cart" class="add-to-cart" title="Add to Cart"><span></span>Add to Cart</button>

          </li>
        </ul>
      </div>
      <div class="detail-inner-left right-side">
        <ul>

          <li class="pro-wishlist-icon"><a role="button" class="wishlist" title="Wishlist" data-product-id="{{ $product->id }}"></a></li>
          {{-- <li class="pro-compare-icon"><a href="compare.html" title="Compare"></a></li> --}}
        </ul>
      </div>
    </div>
  </div>
  <div class="product-item-details">
    <div class="product-item-name">
      <a href="{{ url('product/'.$product->id) }}">{{ $product->name }}</a>

      {{-- <select class="product__color" name="" id="prod_color_{{$product->id}}">
        @foreach ($product->colors as $key => $color)
          <option value="{{ $color->id }}">{{ $color->name }}</option>
        @endforeach
      </select> --}}
    </div>
    <div class="price-box">
      @if($discount != NULL)
        <del class="price old-price">${{ $product->price }}</del>
        <span class="price">${{ ProductHelper::round_up(($product->price - ($product->price * $discount)), 2) }}</span>
      @else
        <span class="price">${{ $product->price }}</span>
      @endif
    </div>
    <div class="rating-summary-block right-side">
      <div title="53%" class="rating-result"> <span style="width:53%"></span> </div>
    </div>
  </div>
</div>
