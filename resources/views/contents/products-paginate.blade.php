<input type="hidden" name="" id="products_count" value="{{ count($products) }}">
<div class="inner-listing">
  <div class="row" id="" style=" ">
    @if(count($products) == 0)
      <p class="no_products">There are no products in this category</p>
    @else
      @include('contents.product-item', ['products' => $products])
    @endif

  </div><!-- End row -->

  <!-- Buttons paginator -->
  {{-- <div class="row">
    <input type="hidden" name="current_offset" id="current_offset" value="0">
    <input type="hidden" name="total_pages" id="total_pages" value="{{ $paginate }}">
    <input type="hidden" name="current_page" id="current_page" value="1">
    <div class="col-xs-12">
      <div class="pagination-bar">
        <ul>
          <li><a class="paginate__nav" data-navigation="prev"><i class="fa fa-angle-left"></i></a></li>
          @for ($i=0; $i < $paginate; $i++)
            @if ($i==0)
              <li class="active"><a class="btn__paginate" data-offset="0" data-bid="{{ $i+1 }}">{{ $i+1 }}</a></li>
            @else
              <li><a class="btn__paginate" data-offset="{{ $shown*($i) }}" data-bid="{{ $i+1 }}">{{ $i+1 }}</a></li>
            @endif
          @endfor
          <li><a class="paginate__nav" data-navigation="next"><i class="fa fa-angle-right"></i></a></li>
        </ul>
      </div>
    </div>
  </div> --}}
  <!-- End buttons paginator -->

</div>
