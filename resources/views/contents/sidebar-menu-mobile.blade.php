<div class="sidebar-menu-dropdown home">
  <a class="btn-sidebar-menu-dropdown"><span></span> Categories </a>
  <div id="cat" class="cat-dropdown">
    <div class="sidebar-contant">
      <div id="menu" class="navbar-collapse collapse">
        <div class="middle-link mobile right-side">
          <ul>
            <li class="login-icon content">
              <a class="content-link">
              <span class="content-icon"></span>
              </a>
              <a href="{{ url('/login') }}" title="Login">Login</a> or
              <a href="{{ url('/register') }}" title="Register">Register</a>
              <div class="content-dropdown">
                <ul>
                  <li class="login-icon"><a href="{{ url('/login') }}" title="Login"><i class="fa fa-user"></i> Login</a></li>
                  <li class="register-icon"><a href="{{ url('/register') }}" title="Register"><i class="fa fa-user-plus"></i> Register</a></li>
                </ul>
              </div>
            </li>
            <li class="track-icon">
              <a title="Track your order"><span></span> Track your order</a>
            </li>
            <li class="gift-icon">
              <a href="#" title="Gift card"><span></span> Gift card</a>
            </li>
          </ul>
        </div>
        <ul class="nav navbar-nav ">
          {{-- <li class="level sub-megamenu">
            <span class="opener plus"></span>
            <a href="shop.html" class="page-scroll"><i class="fa fa-female"></i>Fashion (10)</a>
            <div class="megamenu mobile-sub-menu">
              <div class="megamenu-inner-top">
                <ul class="sub-menu-level1">
                  <li class="level2">
                    <a href="shop.html"><span>Kids Fashion</span></a>
                    <ul class="sub-menu-level2 ">
                      <li class="level3"><a href="shop.html"><span>■</span>Blazer &amp; Coat</a></li>
                      <li class="level3"><a href="shop.html"><span>■</span>Sport Shoes</a></li>
                      <li class="level3"><a href="shop.html"><span>■</span>Phone Cases</a></li>
                      <li class="level3"><a href="shop.html"><span>■</span>Trousers</a></li>
                      <li class="level3"><a href="shop.html"><span>■</span>Purse</a></li>
                      <li class="level3"><a href="shop.html"><span>■</span>Wallets</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </li>
          <li class="level">
            <a href="shop.html" class="page-scroll"><i class="fa fa-camera-retro"></i>Cameras (70)</a>
          </li>
          <li class="level hidden-md">
            <a href="shop.html" class="page-scroll"><i class="fa fa-desktop"></i>Laptop &amp; computers (10)</a>
          </li>
          <li class="level sub-megamenu">
            <span class="opener plus"></span>
            <a href="shop.html" class="page-scroll"><i class="fa fa-clock-o"></i>Wathches (15)</a>
              <div class="megamenu mobile-sub-menu">
                <div class="megamenu-inner-top">
                  <ul class="sub-menu-level1">
                    <li class="level2">
                      <a href="shop.html"><span>Women Clothings</span></a>
                      <ul class="sub-menu-level2">
                        <li class="level3"><a href="shop.html"><span>■</span>Dresses</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Sport Jeans</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Skirts</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Tops</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Sleepwear</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Jeans</a></li>
                      </ul>
                    </li>
                    <li class="level2">
                      <a href="shop.html"><span>Women Fashion</span></a>
                      <ul class="sub-menu-level2 ">
                        <li class="level3"><a href="shop.html"><span>■</span>Blazer &amp; Coat</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Sport Shoes</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Phone Cases</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Trousers</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Purse</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Wallets</a></li>
                      </ul>
                    </li>
                    <li class="level2 hidden-xs hidden-sm">
                      <a href="shop.html">
                        <img src="images/drop_banner.jpg" alt="Electrro">
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
          </li>
          <li class="level hidden-md">
            <a href="shop.html" class="page-scroll"><i class="fa fa-shopping-bag"></i>Bags (18)</a>
          </li>
          <li class="level sub-megamenu ">
            <span class="opener plus"></span>
            <a href="shop.html" class="page-scroll"><i class="fa fa-tablet"></i>Smartphones (20)</a>
            <div class="megamenu mobile-sub-menu">
                <div class="megamenu-inner-top">
                  <ul class="sub-menu-level1">
                    <li class="level2">
                      <a href="shop.html"><span>Women Clothings</span></a>
                      <ul class="sub-menu-level2">
                        <li class="level3"><a href="shop.html"><span>■</span>Dresses</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Sport Jeans</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Skirts</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Tops</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Sleepwear</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Jeans</a></li>
                      </ul>
                    </li>
                    <li class="level2">
                      <a href="shop.html"><span>Women Fashion</span></a>
                      <ul class="sub-menu-level2 ">
                        <li class="level3"><a href="shop.html"><span>■</span>Blazer &amp; Coat</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Sport Shoes</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Phone Cases</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Trousers</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Purse</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Wallets</a></li>
                      </ul>
                    </li>
                    <li class="level2">
                      <a href="shop.html"><span>Women Fashion</span></a>
                      <ul class="sub-menu-level2 ">
                        <li class="level3"><a href="shop.html"><span>■</span>Blazer &amp; Coat</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Sport Shoes</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Phone Cases</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Trousers</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Purse</a></li>
                        <li class="level3"><a href="shop.html"><span>■</span>Wallets</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
            </div>
          </li>
          <li class="level hidden-md">
            <a href="shop.html" class="page-scroll"><i class="fa fa-heart"></i>Software</a>
          </li>
          <li class="level hidden-md hidden-cet"><a href="shop.html" class="page-scroll"><i class="fa fa-headphones"></i>Headphone (12)</a></li>
          <li class="level">
            <a href="shop.html" class="page-scroll"><i class="fa fa-microphone"></i>Accessories (70)</a>
          </li>
          <li class="level hidden-md hidden-cet"><a href="shop.html" class="page-scroll"><i class="fa fa-bolt"></i>Printers &amp; Ink</a></li>
          <li class="level"><a href="shop.html" class="page-scroll"><i class="fa fa-plus-square"></i>More Categories</a></li> --}}
          @if(Auth::check())
            <li class="level" style="background: #414567;">
              <a href="#" class="page-scroll"><i class="fa fa-user"></i>{{ Auth::user()->name }}</a>
            </li>
          @endif
          @foreach($categories as $category)
            <li class="level">
              <a href="{{ url('products/category/'.$category->id) }}" class="page-scroll"><i class="{{ $category->icon }}"></i>{{ $category->name }}</a>
            </li>
          @endforeach
        </ul>
        <div class="header-top mobile">
          <div class="">
            <div class="row">
              <div class="col-xs-12">
                <div class="top-link top-link-left ">
                  {{-- <ul>
                    <li class="language-icon">
                      <select>
                        <option selected="selected" value="">English</option>
                        <option value="">French</option>
                        <option value="">German</option>
                      </select>
                    </li>
                    <li class="sitemap-icon">
                      <select>
                        <option selected="selected" value="">USD</option>
                        <option value="">AUD</option>
                        <option value="">EUR</option>
                      </select>
                    </li>
                  </ul> --}}
                </div>
              </div>
              <div class="col-xs-12">
                <div class="top-link right-side">
                  {{-- <div class="help-num">Need Help? : 023 233 455 55</div> --}}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
