<section class="mb-60">
            <div class="">
              <div class="product-listing">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="heading-part mb-30">
                      <h2 class="main_title"><span>Special products</span></h2>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="pro_cat">
                    <div class="owl-carousel pro-cat-slider ">
                      @foreach ($last_products as $key => $product)
                      <div class="item">
                        @include('contents.product')
                      </div>
                      @endforeach
                    </div>
                  </div>
                  </div>
              </div>
            </div>
          </section>
