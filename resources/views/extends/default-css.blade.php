<!-- CSS
  ================================================== -->
<!-- <link href="{{ asset('css/styles.css') }}" rel="stylesheet"> -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/fotorama.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/magnific-popup.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">
<link rel="stylesheet" href="{{ asset('css/header.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.min.css') }}">
<!-- <link rel="shortcut icon" href="images/favicon.png"> -->
<!--<link rel="apple-touch-icon" href="images/apple-touch-icon.html">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.html">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.html">
-->
<style>
  .header-top .top-link-left ul li {
    margin-right: 15px;
    font-size: 13px;
  }
  .ml-2, .mx-2 {
    margin-left: .5rem!important;
  }

  /*.carousel-inner>.item>a>img, .carousel-inner>.item>img {
    line-height: 1;
    max-height: 510px !important;
    width: 100%;
  }*/
  .full-width{
    width: 100%;
  }
</style>
