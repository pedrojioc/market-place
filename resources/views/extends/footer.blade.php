<!-- FOOTER START -->
<div class="footer">
  <div class="container">
    <div class="footer-inner">
      <div class="footer-middle">
        <div class="row mb-60">
          <div class="col-md-4 f-col footer-middle-left">
            <div class="f-logo">
              <a href="/" class="">
                <img src="{{ asset('images/logopage.png') }}" alt="Grunem">
              </a>
            </div>
            <div class="footer-static-block"> <span class="opener plus"></span>
              <ul class="footer-block-contant address-footer">
                <li class="item"> <i class="fa fa-map-marker"> </i>
                  <p>Marseille international business center.Tancun road., tian he district. Guangzhou,china.</p>
                </li>
                <li class="item"> <i class="fa fa-envelope"> </i>
                  <p> <a>support@grunem.com </a> </p>
                </li>

              </ul>
            </div>
          </div>
          <div class="col-md-8 footer-middle-right">
            <div class="row">
              <div class="col-md-4 f-col">
                <div class="footer-static-block"> <span class="opener plus"></span>
                  <h3 class="title">@lang('footer.help.0')<span></span></h3>
                  <ul class="footer-block-contant link">

                    <li><a href="{{ asset('documents/privacy-policies_es.pdf') }}" target="_blank">@lang('footer.help.1')</a></li>

                  </ul>
                </div>
              </div>
              <div class="col-md-8 f-col">
                <div class="footer-static-block"> <span class="opener plus"></span>
                  <h3 class="title">@lang('footer.contract.0')<span></span></h3>
                  <ul class="footer-block-contant link">
                    @lang('footer.contract.1')
                  </ul>
                </div>
              </div>
              <!--<div class="col-md-4 f-col">-->
                {{-- <div class="footer-static-block"> <span class="opener plus"></span>
                  <h3 class="title">Information <span></span></h3>
                  <ul class="footer-block-contant link">
                    <li><a> Lorem ipsum daily</a></li>
                    <li><a> Lorem ipsum dolor</a></li>
                    <li><a> Lorem ipsum dolor sit</a></li>
                    <li><a>Lorem ipsum</a></li>
                    <li><a> Lorem ipsum</a></li>
                  </ul>
                </div> --}}
                <!--</div>-->
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div class="footer-bottom ">
          <div class="row mtb-30">
            <div class="col-md-6 ">
              <div class="copy-right ">© 2018 All Rights Reserved. By <a href="#">Grunem</a></div>
            </div>
            <div class="col-md-6 ">
              <div class="footer_social pt-xs-15 center-sm">
                {{-- <ul class="social-icon">
                  <li><a title="Facebook" class="facebook"><i class="fa fa-facebook"> </i></a></li>
                  <li><a title="Twitter" class="twitter"><i class="fa fa-twitter"> </i></a></li>
                  <li><a title="Linkedin" class="linkedin"><i class="fa fa-linkedin"> </i></a></li>
                  <li><a title="RSS" class="rss"><i class="fa fa-rss"> </i></a></li>
                  <li><a title="Pinterest" class="pinterest"><i class="fa fa-pinterest"> </i></a></li>
                </ul> --}}
              </div>
            </div>
          </div>
          <hr>
          <div class="row mtb-30">
            <div class="col-lg-8 col-md-12 ">
              <div class="site-link">
                {{-- <ul>
                  <li><a>About Us</a></li>
                  <li><a>Contact Us</a></li>
                  <li><a>Customer </a></li>
                  <li><a>Service</a></li>
                  <li><a>Privacy</a></li>
                  <li><a>Policy </a></li>
                  <li><a>Accessibility</a></li>
                  <li><a>Directory </a></li>
                </ul> --}}
              </div>
            </div>
            <div class="col-lg-4 col-md-12 ">
              <div class="payment">
                <ul class="payment_icon">

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="scroll-top">
    <div id="scrollup"></div>
  </div>
  <!-- FOOTER END -->
