<!-- HEADER START -->
<header class="navbar navbar-custom container-full-sm" id="header">

  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col-xs-6">
          <div class="top-link top-link-left">
            <ul>
              <li >
                <!-- <select>
                  <option selected="selected" value="">English</option>
                  <option value="">French</option>
                  <option value="">German</option>
                </select> -->
                <a href="https://beta.grunem.com" target="_blank">@lang('menu.top_links.0')</a>
              </li>





              <li >
                <!-- <select>
                  <option selected="selected" value="">USD</option>
                  <option value="">AUD</option>
                  <option value="">EUR</option>
                </select> -->
                Today's Deals
              </li>

              <li >
                Help
              </li>
            </ul>
          </div>
        </div>
        <div class="col-xs-6">
          <div class="top-link right-side">
            <div class="help-num" >@lang('menu.top_links.3')<i class="fa fa-heartbeat"></i></div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="header-middle">
    <div class="container">

      <div class="header-inner">
        <div class="row">
          <div class="col-lg-2 col-md-2">
            <div class="header-middle-left">
              <div class="navbar-header float-none-sm">
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button"><i class="fa fa-bars"></i></button>
                <a class="navbar-brand page-scroll" href="{{url('/')}}">
                  <img alt="Grunem-logo" class="logo-header" src="{{ asset('images/logopage.png') }}">
                </a>
              </div>
            </div>
          </div>
          <div class="col-lg-10 col-md-10">
            <div class="header-middle-right">

              <!-- End menu (home, shop, etc) -->
              <!--
              <div class="row">
                <div class="col-lg-7 col-md-9 ">
                  <div class="header-bottom">
                    <div class="position-r">
                      <div class="nav_sec position-r">
                        <div class="mobilemenu-title mobilemenu">
                          <span>Menu</span>
                          <i class="fa fa-bars pull-right"></i>
                        </div>
                        <div class="mobilemenu-content">
                          <ul class="nav navbar-nav" id="menu-main">
                            <li class="active">
                              <a href="index.html">Home</a>
                            </li>
                            <li>
                              <a href="shop.html">Shop</a>
                            </li>
                            <li>
                              <a href="about.html">About Us</a>
                            </li>
                            <li>
                              <a href="blog.html">Blog</a>
                            </li>
                            <li>
                              <a href="contact.html">Contact</a>
                            </li>
                            <li class="level">
                              <span class="opener plus"></span>
                              <a class="page-scroll">Pages</a>
                              <div class="megamenu mobile-sub-menu">
                                <div class="megamenu-inner-top">
                                  <ul class="sub-menu-level1">
                                    <li class="level2">
                                      <ul class="sub-menu-level2 ">
                                        <li class="level3"><a href="account.html"><span>■</span>Account</a></li>
                                        <li class="level3"><a href="checkout.html"><span>■</span>Checkout</a></li>
                                        <li class="level3"><a href="compare.html"><span>■</span>Compare</a></li>
                                        <li class="level3"><a href="wishlist.html"><span>■</span>Wishlist</a></li>
                                        <li class="level3"><a href="404.html"><span>■</span>404 Error</a></li>
                                        <li class="level3"><a href="single-blog.html"><span>■</span>Single Blog</a></li>
                                        <li class="level3"><a href="product-page.html"><span>■</span>Product Details</a></li>
                                      </ul>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-5 col-md-3">
                  <div class="middle-link right-side">
                    <ul>
                      <li class="login-icon content">
                        <a class="content-link">
                        <span class="content-icon"></span>
                        </a>
                        <a href="login.html" title="Login">Login</a> or
                        <a href="register.html" title="Register">Register</a>
                        <div class="content-dropdown">
                          <ul>
                            <li class="login-icon"><a href="login.html" title="Login"><i class="fa fa-user"></i> Login</a></li>
                            <li class="register-icon"><a href="register.html" title="Register"><i class="fa fa-user-plus"></i> Register</a></li>
                          </ul>
                        </div>
                      </li>
                      <li class="track-icon">
                        <a title="Track your order"><span></span> Track your order</a>
                      </li>
                      <li class="gift-icon">
                        <a href="#" title="Gift card"><span></span> Gift card</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            -->
              <!-- End menu (home, shop, etc) -->
              <div class="row">
                <div class="col-lg-8 col-md-8">
                  <div class="header-right-part">
                    <div class="category-dropdown">
                      <select id="search-category" name="search-category">
                        <option value="0">@lang('menu.search.0')</option>
                        @foreach ($categories as $key => $category)
                          <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                      </select>
                    </div>
                    <div class="main-search">
                      <div class="header_search_toggle desktop-view">
                        <form method="GET" action="{{ route('search.products') }}">
                          <div class="search-box">
                            <input class="input-text" type="text" name="terms" placeholder="@lang('menu.search.1')">
                            <button class="search-btn"></button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4">
                  <div class="right-side float-left-xs header-right-link">
                    <ul>

                      <li class="compare-icon">
                        <a href="#">
                          <span>
                          @if(!UserHelper::profileComplete())
                            <small id="" class="notification user-notification"></small>
                          @endif
                          </span>
                        </a>
                        <div class="header-link-dropdown user__profile__dropdown">
                          <div class="profile_dropd_body">
                            <img src="{{ asset('images/users/avatar-02.png') }}" width="80" alt="">
                            <p class="user__name">{{ Auth::user()->name }}</p>
                            <div class="user__info_body">
                              <ul class="user__options">
                                <li>
                                  <a href="{{ url('user/account') }}" class="relative">
                                    @lang('menu.dropdown_user.0')
                                    <span>
                                    @if(!UserHelper::profileComplete())
                                      <small class="notification account-notification"></small>
                                    @endif
                                    </span>
                                  </a>
                                </li>
                                <li><a href="{{ route('user.logout') }}">@lang('menu.dropdown_user.1')</a></li>
                              </ul>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </li>

                      <li class="cart-icon">
                        <a href="#">
                          <span> <small id="num_of_prods" class="cart-notification">2</small> </span>
                        </a>
                        <div class="cart-dropdown header-link-dropdown">
                          <ul class="cart-list link-dropdown-list" id="cart_items_header">

                          </ul>
                          <p class="cart-sub-totle">
                            <span class="pull-left">@lang('menu.dropdown_cart.0')</span>
                            <span class="pull-right"><strong class="price-box" >$<span id="_subtotal"></span></strong></span>
                          </p>
                          <div class="clearfix"></div>
                          <div class="mt-20">
                            <!--<a href="cart.html" class="btn-color btn">Cart</a>-->
                            {{-- <a href="cart.html" class="ui__button ui__btn__primary">Cart</a> --}}
                            <!--<a href="checkout.html" class="btn-color btn right-side">Checkout</a>-->
                            <a href="{{ url('order/overview') }}" class="ui__button ui__btn__primary">Checkout</a>
                          </div>
                        </div>
                      </li>

                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- HEADER END -->
