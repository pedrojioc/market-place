@extends('layouts.application')
@section('content')
<!--=====================================slider======================================-->
<section class="slider-home">
  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      @for ($i=0; $i < count($sliders); $i++)
        @if ($i==0)
          <li data-target="#carousel-example-generic" data-slide-to="{{$i}}" class="active"></li>
        @else
          <li data-target="#carousel-example-generic" data-slide-to="{{$i}}"></li>
        @endif
      @endfor
      <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    </ol>

    <!-- Wrapper for slides -->

    <div class="carousel-inner" role="listbox">
      @foreach ($sliders as $key => $slider)
        @if ($key == 0)
          <div class="item active">
            <img src="https://grunem-marketplace.s3-us-west-2.amazonaws.com/{{$slider->image}}" class="img-responsive" alt="{{$slider->alt}}">
            <div class="carousel-caption">

            </div>
          </div>
        @else
          <div class="item">
            <img src="https://grunem-marketplace.s3-us-west-2.amazonaws.com/{{$slider->image}}" class="img-responsive" alt="{{$slider->alt}}">
            <div class="carousel-caption">

            </div>
          </div>
        @endif
        <div class="item">
          <img src="https://grunem-marketplace.s3-us-west-2.amazonaws.com/market-place/home/sliders/SlidersViviNex.png" class="img-responsive" alt="">
          <div class="carousel-caption">

          </div>
        </div>
      @endforeach

    </div>

    <!-- Controls -->
    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</section>
<!--=====================================./slider======================================-->
<!-- This include contain nav categories and slider 2 -->
@include('contents.categories-nav')
<!-- Start -->
<section>
  <!-- container -->
  <div class="container">
    <!-- row -->
    <div class="row">
      <!-- col-lg-12 -->
      <div class="col-lg-12 col-md-12 col-lgmd-100per right-side float-none-sm float-right-imp">

        @include('contents.latest-featured-viewed', [$products, $last_products])
        <!-- small banner block start -->
        <section class="mt-60">
          <div class="row">
            <div class="col-md-6">
              <div class="small-banner small-banner1">
                <a>
                    <img src="images/smallbanner.png" alt="">
                  </a>
              </div>
            </div>
            <div class="col-md-6">
              <div class="small-banner small-banner2">
                <a>
                    <img src="images/smallbanner2.png" alt="Electrro">
                  </a>
              </div>
            </div>
          </div>
        </section>
        <!-- ./small banner block start -->

        <!-- sites services -->
        <div class="ser-feature-block mtb-60">
          <div class="">
            <div class=" center-sm">
              <div class="row">
                <div class="col-lg-4 service-box border-right">
                  <div class="feature-box ">
                    <div class="feature-icon feature1"></div>
                    <div class="feature-detail">
                      <div class="ser-title">Free Shipping</div>
                      <div class="ser-subtitle"> Shipping in World for orders</div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 service-box border-right">
                  <div class="feature-box">
                    <div class="feature-icon feature2"></div>
                    <div class="feature-detail">
                      <div class="ser-title">Special Gift</div>
                      <div class="ser-subtitle">Give the perfect gift to your loved ones </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 service-box">
                  <div class="feature-box ">
                    <div class="feature-icon feature3"></div>
                    <div class="feature-detail">
                      <div class="ser-title">Money Back</div>
                      <div class="ser-subtitle">Not receiving an item applied to reward</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- ./sites services -->

        <!--special products -->
        @include('contents.special-products', $products)
        <!-- ./special products -->

        <!-- sub-banner-block -->
        <div class="sub-banner-block">
          <div class="">
            <div class=" center-sm">
              <div class="row">
                <div class="col-md-4 col-sm-4 ">
                  <div class="banner-top home-video">
                    <div class="sub-banner sub-banner2">
                      <img alt="Electrro" src="{{ asset('images/home/VidThumb.png') }}">
                      <!-- <div class="sub-banner-effect"></div> -->
                      <div class="sub-banner-detail">
                        <div class="video-block">
                          <a class="popup-youtube" href="https://www.youtube.com/watch?v=jD4E-oY0d2A">
                                <img alt="Electrro" src="{{ asset('images/play-button.png') }}" draggable="false">
                            </a>
                        </div>
                        <div class="sub-banner-subtitle">Must See Video </div>
                        {{-- <div class="sub-banner-title sub-banner-title-color">Hurry ! Don’t miss it</div> --}}
                        <!-- <a class="btn btn-color" href="shop.html">Shop Now!</a> -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 ">
                  <div class="banner-top home-video center">
                    <div class="sub-banner sub-banner2">
                      <img alt="Electrro" src="{{ asset('images/home/VivonexThumb.png') }}">
                      <!-- <div class="sub-banner-effect"></div> -->
                      <div class="sub-banner-detail">
                        <div class="video-block">
                          <a class="popup-youtube" href="https://www.youtube.com/watch?v=YCTq9eBje4s">
                                <img alt="Electrro" src="{{ asset('images/play-button.png') }}" draggable="false">
                            </a>
                        </div>
                        <div class="sub-banner-subtitle">Must See Video </div>
                        {{-- <div class="sub-banner-title sub-banner-title-color">Hurry ! Don’t miss it</div> --}}
                        <!-- <a class="btn btn-color" href="shop.html">Shop Now!</a> -->
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-4 ">
                  <div class="banner-top home-video">
                    <div class="sub-banner sub-banner2">
                      <img alt="Electrro" src="{{ asset('images/home/mimix2sThumb.png') }}">
                      <!-- <div class="sub-banner-effect"></div> -->
                      <div class="sub-banner-detail">
                        <div class="video-block">
                          <a class="popup-youtube" href="https://www.youtube.com/watch?v=K0wGp3I1oNw">
                                <img alt="Electrro" src="{{ asset('images/play-button.png') }}" draggable="false">
                            </a>
                        </div>
                        <div class="sub-banner-subtitle">Must See Video </div>
                        {{-- <div class="sub-banner-title sub-banner-title-color">Hurry ! Don’t miss it</div> --}}
                        <!-- <a class="btn btn-color" href="shop.html">Shop Now!</a> -->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- ./sub-banner-block -->
      </div>
      <!-- end-col-lg-12 -->
    </div>
    <!-- end-row -->
  </div>
  <!-- end-container -->
</section>

@endsection

@section('customcss')
  <style media="screen">
    .product-item-name{
      display: flex;
    }
    .product__color{
      padding-top: 2px;
      padding-bottom: 2px;
    }
  </style>
@endsection
