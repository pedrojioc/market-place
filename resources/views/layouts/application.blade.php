<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Grunem Market Place</title>
  <!-- Default properties -->
  @include('extends.default-properties')
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Default Styles -->
  @include('extends.default-css')
  <style media="screen">
  .internal-se-pre-con {
    position: absolute;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url({{ asset('/images/logoGrunem.png') }}) center top no-repeat #fff;
  }
  .notification{
    background: #ec7e00;
    color: #333333;
    border-radius: 50%;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    -o-border-radius: 50%;
    height: 10px;
    line-height: 15px;
    text-align: center;
    width: 10px;
  }
  .user-notification{
    position: absolute;
    top: 0;
    right: 1px;
  }
  .account-notification{
    position: absolute;
    top: 6px;
    right: -15px;
  }
  .relative{
    position: relative;
  }
  </style>
  @yield('customcss')
</head>
<body class="{{ Request::is( '/') ? 'homepage' : '' }}">
  <div class="se-pre-con"></div>

  <div class="main">
    @if(Auth::check())
    @include('extends.header')
    @else
    @include('extends.header-no-logged')
    @endif

    <div id="wrapper">
      @yield('content')
    </div>
  </div>

  <!-- Footer -->
  @include('extends.footer')
</body>
<!-- Default JS -->
@include('extends.default-js')
@yield('custom-js')
</html>
