<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title>Grunem</title>

  <link rel="stylesheet" href="{{ asset('css/register.css') }}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
  <style media="screen">
    body {
      height: 100%;
      direction: ltr;
    }

    .wrapper {
      display: table;
      width: 100%;
      height: 100%;
    }

    .sidebar-form {
      display: table-cell;
      width: 580px;
      height: 100%;
      padding: 20px 80px;
      background-color: #ffffff;
    }

    .cover-page {
      display: table-cell;
      height: 100%;
      background-color: #eff2f7;
      text-align: right;
      vertical-align: top;
      color: #8492a6;
    }

    .cover-page {
      overflow: visible;
      position: relative;
      width: auto;
      margin-left: 0;
      min-height: auto;
      padding: inherit;
    }

    .bg-cover {
      background-image: -webkit-linear-gradient(top, #36404e, #36404e);
      background-image: linear-gradient(to bottom, #36404e, #435063);
      background-color: #36404e;
      background-size: cover !important;
      background-position: center !important;
      background-repeat: no-repeat !important;
    }
    .hero-login{
      height: 100%;
    }
    .form-content{
      text-align: center;
    }
    .logo{
      width: 69px;
    }
    .title{
      font-size: 23px;
      color: #8492a6;
      margin-top: 18px;
      margin-bottom: 6px;
      text-align: left;
      text-transform: uppercase;
      letter-spacing: 1.5px;
    }
    .pharagrap{
      text-align: justify;
      margin-bottom: 25px;
      font-weight: 300;
      color: #8492a6;
    }

    input:focus{
      outline: none !important;
    }
  </style>
  @yield('custom-css')
</head>

<body>
  <div class="wrapper">
    <div class="sidebar-form">
      <div class="form-content">
        <img src="{{ asset('images/isologo.png') }}" alt="" class="logo">
        <div class="title">
          Fill in the data to continue
        </div>
        <p class="pharagrap">
          
        </p>
      </div>
      <div class="app-content">
        @yield('content')
      </div>
    </div>
    <section class="cover-page">
      <div class="hero-login bg-cover" style="background-image: url('{{asset("images/register-2.jpg")}}');">

      </div>
    </section>
  </div>
</body>
<!-- Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@yield('custom-js')
</html>
