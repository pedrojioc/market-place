@extends('layouts.application')
@section('content')
<!-- CONTAIN START -->
<section class="checkout-section ptb-95">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="checkout-step mb-40">
          <ul>
            <li> <a href="#">
              <div class="step">
                <div class="line"></div>
                <div class="circle">1</div>
              </div>
              <span>Shipping</span> </a> </li>
            <li> <a href="#">
              <div class="step">
                <div class="line"></div>
                <div class="circle">2</div>
              </div>
              <span>Order Overview</span> </a> </li>
            <li> <a href="#">
              <div class="step">
                <div class="line"></div>
                <div class="circle">3</div>
              </div>
              <span>Payment</span> </a> </li>
            <li class="active"> <a href="#">
              <div class="step">
                <div class="line"></div>
                <div class="circle">4</div>
              </div>
              <span>Order Complete</span> </a>
            </li>
            <li class="active">
              <div class="step">
                <div class="line"></div>
              </div>
            </li>
          </ul>
          <hr>
        </div>
        <div class="checkout-content">
          <div class="row">
            <div class="col-xs-12">
              <div class="heading-part align-center">
                <h2 class="heading">Order Overview</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-8 mb-sm-30">

              <div class="complete-order-detail commun-table mb-30">
                <div class="table-responsive">
                  <table class="table">
                    <tbody>
                      <tr>
                        <td><b>Order Date :</b></td>
                        <td>{{ $invoice->created_at }}</td>
                      </tr>
                      <tr>
                        <td><b>Total :</b></td>
                        <td><div class="price-box"> <span class="price">${{ $invoice->total - $invoice->discount }}</span> </div></td>
                      </tr>
                      <tr>
                        <td><b>Payment :</b></td>
                        <td>{{$payment_method->name}}</td>
                      </tr>
                      <tr>
                        <td><b>Order No. :</b></td>
                        <td>#{{ $invoice->id }}</td>
                      </tr>
                      <tr>
                        <td><b>Status :</b></td>
                        <td>{{ $status->name }}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="mb-30">
                <div class="heading-part">
                  <h3 class="sub-heading">Order Confirmation</h3>
                </div>
                <hr>
                <p class="mt-20">On this page you can check the detail and status of your order. We will also be informing you of the status of your order.
In case the order is rejected please contact us through our support channels or send a message to support@grunem.com</p>
              </div>
              <div class="right-side float-none-xs"> <a class="btn btn-color" href="{{ url('/') }}"><span><i class="fa fa-angle-left"></i></span>Continue Shopping</a> </div>
            </div>
            <div class="col-sm-4">
              <div class="cart-total-table address-box commun-table mb-30">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Shipping Address</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><ul>
                            <li class="inner-heading"> <b>{{$shipping->city}}</b> </li>
                            <li>
                              <p>{{ $state->name }},</p>
                            </li>
                            <li>
                              <p>{{ $shipping->address }}.</p>
                            </li>
                            <li>
                              <p>{{ $country->name }}</p>
                            </li>
                          </ul></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- CONTAINER END -->
@endsection

@section('custom-js')
  <script type="text/javascript">
    window.localStorage.clear();
  </script>
@endsection
