@extends('layouts.application')
@section('content')
  <!-- CONTAIN START -->
<section class="checkout-section ptb-95">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="checkout-step mb-40">
          <ul>
            <li class="active"> <a href="#">
              <div class="step">
                <div class="line"></div>
                <div class="circle">2</div>
              </div>
              <span>Order Overview</span> </a>
            </li>
            <li> <a href="#">
              <div class="step">
                <div class="line"></div>
                <div class="circle">1</div>
              </div>
              <span>Shipping</span> </a>
            </li>
            <li> <a href="#">
              <div class="step">
                <div class="line"></div>
                <div class="circle">3</div>
              </div>
              <span>Payment</span> </a> </li>
            <li> <a href="#">
              <div class="step">
                <div class="line"></div>
                <div class="circle">4</div>
              </div>
              <span>Order Complete</span> </a> </li>
            <li>
              <div class="step">
                <div class="line"></div>
              </div>
            </li>
          </ul>
          <hr>
        </div>
        <div class="checkout-content">
          <div class="row">
            <div class="col-xs-12">
              <div class="heading-part align-center">
                <h2 class="heading">Order Overview</h2>
              </div>
            </div>
          </div>
          <div class="row d-flex center">
            <div class="col-xs-12">
              <p class="notice">{{ session('notice') }}</p>
            </div>
          </div>
          <div class="row d-flex center">
            <div class="col-sm-8 mb-sm-30">
              <div class="cart-item-table commun-table mb-30">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Product</th>
                        <th>Product Detail</th>
                        <th>Sub Total</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody id="body_overview">

                    </tbody>
                  </table>
                </div>
              </div>
              <div class="cart-total-table commun-table mb-30">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th colspan="2">Cart Total</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Item(s) Subtotal</td>
                        <td><div class="price-box"> <span id="subtotal" class="price"></span> </div></td>
                      </tr>
                      <tr>
                        <td>Shipping</td>
                        <td><div class="price-box"> <span class="price" id="shipping"></span> </div></td>
                      </tr>
                      <tr>
                        <td><b>Amount Payable</b></td>
                        <td><div class="price-box"> <span class="price"><b id="total"></b></span> </div></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <form action="{{ route('invoice.create') }}" method="POST">
                <input type="hidden" id="data" name="data" value="">
                @csrf
                <div class="right-side float-none-xs"> <button type="submit" class="btn btn-color">Next</button> </div>
              </form>
            </div><!-- ./col-sm-8 -->

            {{-- <div class="col-sm-4">
              <div class="cart-total-table address-box commun-table mb-30">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Shipping Address</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><ul>
                            <li class="inner-heading"> <b>Denial tom</b> </li>
                            <li>
                              <p>5-A kadambari aprtment,opp. vasan eye care,</p>
                            </li>
                            <li>
                              <p>Risalabaar,City Road, deesa-405001.</p>
                            </li>
                            <li>
                              <p>India</p>
                            </li>
                          </ul></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="cart-total-table address-box commun-table">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th>Billing Address</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><ul>
                            <li class="inner-heading"> <b>Denial tom</b> </li>
                            <li>
                              <p>5-A kadambari aprtment,opp. vasan eye care,</p>
                            </li>
                            <li>
                              <p>Risalabaar,City Road, deesa-405001.</p>
                            </li>
                            <li>
                              <p>India</p>
                            </li>
                          </ul></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div> --}}<!-- ./col-sm-4 -->
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- CONTAINER END -->
@endsection

@section('customcss')
  <style media="screen">
    .notice{
      text-align: center;
      font-size: 16px;
      color: #f90448;
    }
  </style>
@endsection

@section('custom-js')
  <script type="text/javascript">
  function serializeData(){
    var key = '';
    var data = new Array();
    for(let i = 0; i<=localStorage.length; i++){
      key = localStorage.key(i);
      if(key != null && key.substring(0,5) == 'prod_') {
        cartItem = JSON.parse(localStorage.getItem(key));
        data.push(cartItem);
      }
    }
    $('#data').val(JSON.stringify(data));
    console.log(data);
  }
  $(document).ready(function() {

    function loadOverView(){
      var body_overview = $('#body_overview');
      body_overview.empty();
      var key = '';
      var total_ship = 0;
      var subtotal = 0;
      for(let i = 0; i<=localStorage.length; i++){
        key = localStorage.key(i);
        if(key != null && key.substring(0,5) == 'prod_') {
          cartItem = JSON.parse(localStorage.getItem(key));
          var name = cartItem.name;
          var price = +cartItem.price;
          var amount = price*cartItem.quantity;
          var weight = cartItem.weight * cartItem.quantity;
          var shipping = 0.065 * weight;
          var delCart = "delete_item";
          var overview = '<tr>'+
                            '<td>'+
                              '<a href="product-page.html">'+
                                '<div class="product-image"><img alt="Honour" src="'+cartItem.image+'"></div>'+
                              '</a>'+
                            '</td>'+
                            '<td>'+
                              '<div class="product-title">'+
                                '<a href="product-page.html">'+name+'</a>'+
                                '<div class="product-info-stock-sku m-0">'+
                                  '<div>'+
                                    '<label>Price: </label>'+
                                    '<div class="price-box"> <span class="info-deta price">$'+price+'</span> </div>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="product-info-stock-sku m-0">'+
                                  '<div>'+
                                    '<label>Quantity: </label>'+
                                    '<span class="info-deta">'+cartItem.quantity+'</span>'+
                                  '</div>'+
                                '</div>'+
                              '</div>'+
                            '</td>'+
                            '<td>'+
                              '<div data-id="100" class="total-price price-box"> <span class="price">$'+amount+'</span> </div>'+
                            '</td>'+
                            '<td>'+
                              '<i class="fa fa-trash cart-remove-item" name="delete_item" data-id="'+cartItem.id+'" title="Remove Item From Cart"></i>'+
                            '</td>'+
                        '</tr>';
          total_ship = total_ship + shipping;
        }
      }//End for
      body_overview.append(overview);
      subtotal = parseFloat(localStorage.getItem('total_cost'));
      $('#subtotal').html('$'+subtotal.toFixed(2));
      total_ship = Math.round(total_ship * 100) / 100;
      let total = parseFloat(localStorage.getItem('total_cost'));
      total = total + total_ship;
      $('#shipping').html('$'+total_ship);
      $('#total').html('$'+(total.toFixed(2)));
    }
    loadOverView();
    serializeData();
    $('body').on('click', '.cart-remove-item' , function() { // To delete elements after they've been dynamically updated
      console.log($(this).data('id'));


 				localStorage.removeItem('prod_' + $(this).data('id'));
 				cartRefresh();
        serializeData();
        loadOverView();

    });

  });//End section
  </script>
@endsection
