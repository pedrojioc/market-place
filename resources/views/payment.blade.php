@extends('layouts.application')
@section('content')
  <!-- CONTAIN START -->
<section class="checkout-section ptb-95">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <div class="checkout-step mb-40">
          <ul>
            <li> <a href="#">
              <div class="step">
                <div class="line"></div>
                <div class="circle">2</div>
              </div>
              <span>Order Overview</span> </a>
            </li>
            <li> <a href="#">
              <div class="step">
                <div class="line"></div>
                <div class="circle">1</div>
              </div>
              <span>Shipping</span> </a>
            </li>
            <li class="active"> <a href="#">
              <div class="step">
                <div class="line"></div>
                <div class="circle">3</div>
              </div>
              <span>Payment</span> </a>
            </li>
            <li> <a href="#">
              <div class="step">
                <div class="line"></div>
                <div class="circle">4</div>
              </div>
              <span>Order Complete</span> </a> </li>
            <li>
              <div class="step">
                <div class="line"></div>
              </div>
            </li>
          </ul>
          <hr>
        </div>
        <div class="checkout-content">
          <div class="row">
            <div class="col-xs-12">
              <div class="heading-part align-center">
                <h2 class="heading">Select a payment method</h2>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
              <p class="notice">{{ session('notice') }}</p>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6 col-md-8 col-sm-8 col-lg-offset-3 col-sm-offset-2">
              <div class="payment-option-box mb-30">
                <div class="payment-option-box-inner gray-bg">
                  <div class="payment-top-box">
                    <select class="form-control" name="payment_method" id="select_payment_method">
                      <option value="1">Bank deposit</option>
                      <option value="2">Bitcoin</option>
                    </select>
                  </div>
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="" id="box_payment_form">
                        <form class="" action="{{ route('payment.create') }}" method="POST" name="form_deposit_bank" enctype="multipart/form-data" id="form_deposit_bank">
                          @csrf
                          <input type="hidden" name="invoice_id" value="{{ $invoice->id }}">
                          <input type="hidden" name="payment_method" value="1">
                          <p>Total to pay: <strong>{{ $invoice->total - $invoice->discount }}</strong></p>
                          <p>Number Account: <strong>80-5160-900</strong></p>
                          <p>Bank: <strong>AV VILLAS</strong></p>
                          <p>Account type: <strong>Savings</strong></p>
                          <div class="form-group">
                            <label for="">Bank Name</label>
                            <input type="text" name="bank_name" class="form-control" value="" required>
                          </div>
                          <div class="form-group">
                            <label for="">Swift Code</label>
                            <input type="text" name="swift_code" class="form-control" value="" required>
                          </div>

                          <input type="hidden" name="amount" class="form-control" value="{{ $invoice->total }}" required>

                          <div class="form-group">
                            <label for="message-text" class="form-control-label">Image:</label><br>
                            <input type="file" name="image" id="image"/ required>
                          </div>
                          <div class="right-side float-none-xs">
                            <button class="btn btn-color" type="submit">Send<span><i class="fa fa-angle-right"></i></span></button>
                          </div>
                        </form>
                        <form class="" action="{{ route('payment.create') }}" method="POST" name="form_bitcoin_transaction" id="form_bitcoin_transaction">
                          @csrf
                          <input type="hidden" name="invoice_id" value="{{ $invoice->id }}">
                          <input type="hidden" name="payment_method" value="2">
                          <p class="text-center">Total to pay: <strong>{{ $btc_amount }}</strong> BTC</p>
                          <img src="{{ $qr_code }}" class="qr-code" alt="QR CODE">
                          <p class="text-center"><strong>14tPwpJU8cofdLd3qU7EFwec3CqK43ZsoV</strong></p>


                          <input type="hidden" name="amount" class="form-control" value="{{ $btc_amount }}" required>

                          <div class="form-group">
                            <label for="">Transaction ID</label>
                            <input type="text" name="transaction_id" class="form-control" value="" required>
                          </div>
                          <div class="right-side float-none-xs">
                            <button class="btn btn-color" type="submit">Send<span><i class="fa fa-angle-right"></i></span></button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- CONTAINER END -->
@endsection

@section('customcss')
  <style media="screen">
    .notice{
      text-align: center;
      font-size: 16px;
      color: #f90448;
    }
    #form_bitcoin_transaction{
      display: none;
    }
    .text-center{
      text-align: center;
    }
    .qr-code{
      display: block;
      margin-left: auto;
      margin-right: auto;
    }
  </style>
@endsection

@section('custom-js')

  <script type="text/javascript">
    $( "#select_payment_method" ).change(function() {
      if($(this).val() == 2){
        $('#form_deposit_bank').css('display', 'none');
        $('#form_bitcoin_transaction').css('display', 'block');
      }else{
        $('#form_bitcoin_transaction').css('display', 'none');
        $('#form_deposit_bank').css('display', 'block');
      }
    });
  </script>
@endsection
