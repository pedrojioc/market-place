@extends('layouts.application')
@section('content')
  <!-- Top banner -->
  <div class="banner inner-banner1 " style="background: url({{asset('/images/inner-banner2.jpg')}}) no-repeat scroll center center;">
    <div class="container">
      <section class="banner-detail center-xs">
        <h1 class="banner-title">{{ $category->name }}</h1>
        <div class="bread-crumb right-side float-none-xs">
          <ul>
            <li><a href="{{ url('/') }}">Home</a>/</li>
            <li><span>{{ $category->name }}</span></li>
          </ul>
        </div>
      </section>
    </div>
  </div>
  <!-- ./Top banner -->
  <!-- Sidebar Mobile -->
  @include('contents.sidebar-menu-mobile')
  <!-- ./Sidebar Mobile -->
  <section class="ptb-95">
    @csrf
    <div class="container">
      <input type="hidden" name="category_id" id="category_id" value="{{ $category_id }}">
      <div class="row">
        <div class="col-lg-2 col-md-3 mb-sm-30 col-lgmd-20per">
          <div class="sidebar-block">
            <div class="sidebar-box listing-box mb-40"> <span class="opener plus"></span>
              <div class="sidebar-title">
                <h3>Categories</h3> <span></span>
              </div>
              <div class="sidebar-contant">
                <ul>
                  @foreach ($categories as $category)
                    <li><a>{{ $category->name }}</a></li>
                  @endforeach

                  {{-- <li><a>Shoes <span>(05)</span></a></li>
                  <li><a>Jewellery <span>(10)</span></a></li>
                  <li><a>Furniture <span>(12)</span></a></li>
                  <li><a>Bags <span>(18)</span></a></li>
                  <li><a>Accessories <span>(70)</span></a></li> --}}
                </ul>
              </div>
            </div>
            <div class="sidebar-box mb-40"> <span class="opener plus"></span>
              <div class="sidebar-title">
                <h3>Shop by</h3> <span></span>
              </div>
              <div class="sidebar-contant">
                <div class="price-range mb-30">
                  <div class="inner-title">Price range</div>
                  <input class="price-txt" type="text" id="amount">
                  <div id="slider-range">
                  </div>
                  {{-- <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                    <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 9.375%; width: 53.125%;"></div>
                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 9.375%;"></span>
                    <span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 62.5%;"></span>
                  </div> --}}
                </div>
                <div class="size mb-20">

                </div>
                <div class="mb-20">
                  <div class="inner-title">Color</div>
                  <br>
                  {{-- <ul>
                    <li><a>Black <span>(0)</span></a></li>
                    <li><a>Blue <span>(05)</span></a></li>
                    <li><a>Brown <span>(10)</span></a></li>
                  </ul> --}}
                  <div class="" style="min-height:35px;">
                    <div class="select-item">
                      <select class="" id="colors" name="colors">
                        <option value="0" selected>All</option>
                        @foreach ($colors as $key => $color)
                          <option value="{{ $color->id }}">{{ $color->name }}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
                <div class="mb-20">

                </div>
                {{-- <a href="#" class="btn btn-color">Refine</a> --}}
              </div>
            </div>


          </div>
        </div>
        <div class="col-lg-10 col-md-9 col-lgmd-80per">

          <div class="shorting mb-30">
            <div class="row">
              <div class="col-md-6">
                <div class="view">
                  <div class="list-types grid active ">
                    <a href="#">
                      <div class="grid-icon list-types-icon"></div>
                    </a>
                  </div>
                  <div class="list-types list">
                    {{-- <a href="shop-list.html">
                    <div class="list-icon list-types-icon"></div>
                    </a>  --}}
                  </div>
                </div>
                <div class="short-by float-right-sm"> <span>Sort By</span>
                  <div class="select-item">
                    <select id="order_products">
                      <option value="1" selected="selected">Name (A to Z)</option>
                      <option value="2">Name(Z - A)</option>
                      <option value="3">price(low&gt;high)</option>
                      <option value="4">price(high &gt; low)</option>
                      {{-- <option value="">rating(highest)</option>
                      <option value="">rating(lowest)</option> --}}
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="show-item right-side float-left-sm"> <span>Show</span>
                  <div class="select-item">
                    <select name="number_p_show" id="number_p_show">
                      @if ($shown == 10)
                        <option value="10" selected="selected">10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                      @elseif ($shown == 25)
                        <option value="10">10</option>
                        <option value="25" selected="selected">25</option>
                        <option value="50">50</option>
                      @else
                        <option value="10">10</option>
                        <option value="25">25</option>
                        <option value="50" selected="selected">50</option>
                      @endif
                    </select>
                  </div>
                  <span>Per Page</span>

                </div>
              </div>
            </div>
          </div>

          <div class="product-listing">
            <div class="" id="content_products">
              <div class="internal-se-pre-con" style="display:none"></div>
              @include('contents.products-paginate')
            </div>

            <!-- Buttons paginator -->
            <div class="row">
              <input type="hidden" name="current_offset" id="current_offset" value="0">
              <input type="hidden" name="total_pages" id="total_pages" value="{{ $paginate }}">
              <input type="hidden" name="current_page" id="current_page" value="1">
              <div class="col-xs-12">
                <div id="paginator" class="pagination-bar">
                  <ul>
                    <li><a id="pagination_prev" class="paginate__nav" data-offset="-10" data-navigation="prev"><i class="fa fa-angle-left"></i></a></li>
                    @for ($i=0; $i < $paginate; $i++)
                      @if ($i==0)
                        <li class="active"><a class="btn__paginate" data-offset="0" data-bid="{{ $i+1 }}">{{ $i+1 }}</a></li>
                      @else
                        <li><a class="btn__paginate" data-offset="{{ $shown*($i) }}" data-bid="{{ $i+1 }}">{{ $i+1 }}</a></li>
                      @endif
                    @endfor
                    <li><a id="pagination_next" class="paginate__nav" data-offset="10" data-navigation="next"><i class="fa fa-angle-right"></i></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- End buttons paginator -->
          </div>

        </div>
      </div>
    </div>
  </section>

@endsection

@section('customcss')

<style media="screen">
  .no_products{
    font-size: 24px;
    text-align: center;
    font-size: 37px;
    color: rgba(16, 16, 16, 0.42);
  }

  .loader{
    position:fixed;
    padding:0;
    margin:0;

    top:0;
    left:0;

    width: 100%;
    height: 100%;
    background:rgba(255,255,255,0.5);
  }

</style>
@endsection
@section('custom-js')
  <script type="text/javascript">
    //This function make a request ajax
    async function doRequestFilter( data ){
      let result;
      try {
        result = await $.ajax({
          type: 'POST',
          url: '/products/category/filtered',
          data: {
            '_token': $('input[name=_token]').val(),
            'order': data.order,
            'limit': data.limit,
            'start': data.offset,
            'category_id': data.category,
            'color': data.color,
            'price_min': data.priceMin,
            'price_max': data.priceMax
          }
        })
      } catch (e) {
        //Debug true only in development
        console.log('Error:');
        console.log('Status: '+e.statusText);
      }

      return result;
    }

    //This function print the products and change the values of the paginator
    function printHtmlProducts(result){
      $('#content_products').html(result.body);
      $('#current_offset').val(data.offset);
    }

    function makePaginate({paginate, shown}){

      let html_paginator = '<ul><li><a id="pagination_prev" class="paginate__nav" data-offset="-10" data-navigation="prev"><i class="fa fa-angle-left"></i></a></li>';
        for(let i=0; i < paginate; i++){
          if (i==0){
            html_paginator += '<li class="active"><a class="btn__paginate" data-offset="0" data-bid="'+(i+1)+'">'+(i+1)+'</a></li>';
          }else{
            html_paginator += '<li><a class="btn__paginate" data-offset="'+(shown*i)+'" data-bid="'+(i+1)+'">'+(i+1)+'</a></li>';
          }
        }
        html_paginator += '<li><a id="pagination_next" class="paginate__nav" data-offset="10" data-navigation="next"><i class="fa fa-angle-right"></i></a></li>'+
      '</ul>';

      return html_paginator;
    }

    $(document).ready(function() {

      //Global variables in context ready
      var products_count = $('#products_count').val();
      //Object config
      let config = {
        category: document.getElementById('category_id').value,
        color: null,
        totalPages: parseInt(document.getElementById('total_pages').value),
        order : 1,
        limit: parseInt(document.getElementById('number_p_show').value),
        currentpage: 1,
        currentOffset: 0,
        offset: 0,
        priceMin: 5,
        priceMax: 1000
      }

      if(products_count == 0){
        $('.pagination-bar').css('display', 'none');
      }else{
        $('.pagination-bar').css('display', 'block');
      }

      //Select order products
      $('#order_products').change(function(){

        $('.se-pre-con').css('display', 'block');

        doRequestFilter(config).then( (response) => {
          console.log(response);
          $('#content_products').html(response.body);
          $('.se-pre-con').fadeOut('slow');
        } )
      });

      //Number of Products show
      $('#number_p_show').change(function(){
        config.limit = document.getElementById('number_p_show').value;
        let url = '{{ url('/products/category') }}'
        url += `/${config.category}/${config.limit}`;
        console.log(url);
        location.href = url;
      });

      //Filter by color
      $('#colors').change(function(){
        $('.se-pre-con').css('display', 'block');
        config.color = (parseInt($(this).val()) === 0) ? null : parseInt($(this).val());
        config.offset = 0;
        doRequestFilter(config).then( (response) => {
          console.log(response);
          $('#content_products').html(response.body);
          config.currentOffset = config.offset;

          config.totalPages = response.paginate;

          $('#paginator').html(makePaginate(response));
          $('.se-pre-con').fadeOut('slow');
        } )
      });


      $(document).on('click', '.btn__paginate', function(e) {
        e.preventDefault();
        $('.se-pre-con').css('display', 'block');
        let _this = this;
        config.offset = parseInt($(this).data('offset'));

        doRequestFilter(config).then( (response) => {
          $('#content_products').html(response.body);
          config.currentOffset = config.offset;

          $('.btn__paginate').parent().removeClass('active');
          $(_this).parent().addClass('active');

          $('#pagination_next').data('offset', (config.currentOffset + config.limit));
          $('#pagination_prev').data('offset', (config.currentOffset - config.limit));
          window.scrollTo(0,screen.availHeight * 0.38);
          $(".se-pre-con").fadeOut("slow");
        } )
      });

      $(document).on('click', '.paginate__nav', function(e){
        e.preventDefault();

        config.offset = parseInt($(this).data('offset'));
        if(config.offset==(config.totalPages * config.limit) || config.offset < 0){
          console.log('Block');
          return;
        }

        $('.se-pre-con').css('display', 'block');


        doRequestFilter(config).then( (response) => {
          $('#content_products').html(response.body);
          config.currentOffset = config.offset;

          $('.btn__paginate').parent().removeClass('active');
          $("[data-offset='"+config.currentOffset+"']").parent().addClass('active');
          $('#pagination_prev').parent().removeClass('active');
          $('#pagination_next').parent().removeClass('active');

          $('#pagination_next').data('offset', (config.currentOffset + config.limit));
          $('#pagination_prev').data('offset', (config.currentOffset - config.limit));
          window.scrollTo(0,screen.availHeight * 0.38);
          $(".se-pre-con").fadeOut("slow");
        } )
      });

      //Slider range
      $( "#slider-range" ).slider({
        range: true,
        min: 1,
        max: 1000,
        values: [ 5, 1000 ],
        slide: function( event, ui ) {
          $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
        },
        change: function( event, ui ) {
          console.log('Now execute request');

          $('.se-pre-con').css('display', 'block');
          config.offset = 0;
          config.priceMin = ui.values[0];
          config.priceMax = ui.values[1];
          doRequestFilter(config).then( (response) => {
            $('#content_products').html(response.body);
            config.currentOffset = config.offset;

            config.totalPages = response.paginate;

            $('#paginator').html(makePaginate(response));

            window.scrollTo(0,screen.availHeight * 0.38);
            $(".se-pre-con").fadeOut("slow");
          } )
        }
      });
      $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
        " - $" + $( "#slider-range" ).slider( "values", 1 ) );

    });//End ready

  </script>
@endsection
