@extends('layouts.application')
@section('content')
  <!-- CONTAIN START -->
<section class="ptb-95">
  <div class="container">
    <div class="row">
      <div class="col-lg-2 col-md-3 mb-sm-30 col-lgmd-20per">
        <div class="sidebar-block">
          <div class="sidebar-box listing-box mb-40"> <span class="opener plus"></span>
            <div class="sidebar-title">
              <h3>Categories</h3><span></span>
            </div>
            <div class="sidebar-contant">
              <ul>
                @foreach ($categories as $key => $category)
                  <li><a href="{{ url('products/category/'.$category->id) }}">{{$category->name}}</a></li>
                @endforeach
              </ul>
            </div>
          </div>
          <div class="sidebar-box mb-40"> <span class="opener plus"></span>
            <div class="sidebar-title">
              <h3>Shop by</h3><span></span>
            </div>
            <div class="sidebar-contant">
              <div class="price-range mb-30">
                <div class="inner-title">Price range</div>
                <input class="price-txt" type="text" id="amount">
                <div id="slider-range"></div>
              </div>
              <div class="size mb-20">

              </div>
              <div class="mb-20">
                <div class="inner-title">Color</div>
                <ul>
                  <li><a>Black <span>(0)</span></a></li>
                  <li><a>Blue <span>(05)</span></a></li>
                  <li><a>Brown <span>(10)</span></a></li>
                </ul>
              </div>
              <div class="mb-20">

              </div>
              <a href="#" class="btn btn-color">Refine</a> </div>
          </div>
          <div class="sidebar-box mb-40 visible-md visible-lg">

          </div>
          <div class="sidebar-box sidebar-item"> <span class="opener plus"></span>

          </div>
        </div>
      </div>
      <div class="col-lg-10 col-md-9 col-lgmd-80per">

        <div class="shorting mb-30">
          <div class="row">
            <div class="col-md-6">
              <div class="view">
                <div class="list-types grid"> <a href="shop.html">
                  <div class="grid-icon list-types-icon"></div>
                  </a> </div>
                <div class="list-types list active "> <a href="shop-list.html">
                  <div class="list-icon list-types-icon"></div>
                  </a> </div>
              </div>
              <div class="short-by float-right-sm"> <span>Sort By</span>
                <div class="select-item">
                  <select>
                    <option value="" selected="selected">Name (A to Z)</option>
                    <option value="">Name(Z - A)</option>
                    <option value="">price(low&gt;high)</option>
                    <option value="">price(high &gt; low)</option>
                    <option value="">rating(highest)</option>
                    <option value="">rating(lowest)</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="show-item right-side float-left-sm"> <span>Show</span>
                <div class="select-item">
                  <select>
                    <option value="" selected="selected">24</option>
                    <option value="">12</option>
                    <option value="">6</option>
                  </select>
                </div>
                <span>Per Page</span>

              </div>
            </div>
          </div>
        </div>
        <div class="product-listing list-type">
          <div class="row">
            @foreach ($products as $key => $product)
              <!-- Start product -->
              <div class="col-lg-6 col-xs-12">
                <!-- Data -->
                <input type="hidden" id="prod_name_{{$product->id}}" name="" value="{{ $product->name }}">
                @if($discount != NULL)
                  <input type="hidden" id="prod_price_{{$product->id}}" name="" value="{{ $product->price - ($product->price * $discount) }}">
                @else
                  <input type="hidden" id="prod_price_{{$product->id}}" name="" value="{{ $product->price }}">
                @endif
                <input type="hidden" id="prod_quantity_{{$product->id}}" name="" value="1">
                <input type="hidden" id="prod_image_{{$product->id}}" name="" value="{{ asset('images/products/'.$product->id.'/'.$product->cover_image) }}">
                <!-- End Data -->
                <div class="shop-list-view">
                  <div class="product-item">
                    <div class="product-image"> <a href="product-page.html"> <img src="{{ asset('images/products/'.$product->id.'/'.$product->cover_image) }}" alt=""> </a> </div>
                  </div>
                  <div class="product-item-details">
                    <div class="product-item-name">
                      <a href="product-page.html">{{ $product->name }}</a>
                    </div>
                    <div class="rating-summary-block">
                      <div title="53%" class="rating-result"> <span style="width:53%"></span> </div>
                    </div>

                    <div class="price-box">
                      @if($discount != NULL)
                      <span class="price">${{ $product->price - ($product->price * $discount) }}</span>
                      <del class="price old-price">${{ $product->price }}</del>
                      @else
                        <span class="price">${{ $product->price }}</span>
                      @endif
                    </div>
                    <p>{{ $product->short_description }}</p>
                    <div class="bottom-detail">
                      <ul>
                        <li class="pro-cart-icon">

                          <button id="{{$product->id}}" name="add-to-cart" class="add-to-cart" title="Add to Cart" class=""><span></span></button>

                        </li>
                        <li class="pro-wishlist-icon active"><a href="wishlist.html" title="Wishlist"><span></span></a></a></li>

                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End product -->
            @endforeach

          </div><!-- End row -->
          <div class="row">
            <div class="col-xs-12">
              {{-- <div class="pagination-bar">
                <ul>
                  <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                  <li class="active"><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
                </ul>
              </div> --}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- CONTAINER END -->
@endsection
