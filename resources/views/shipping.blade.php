@extends('layouts.application')
@section('content')

  <!-- CONTAIN START -->
  <section class="checkout-section ptb-95">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <div class="checkout-step mb-40">
            <ul>
              <li> <a href="#">
                <div class="step">
                  <div class="line"></div>
                  <div class="circle">2</div>
                </div>
                <span>Order Overview</span> </a>
              </li>
              <li class="active"> <a href="#">
                <div class="step">
                  <div class="line"></div>
                  <div class="circle">1</div>
                </div>
                <span>Shipping</span> </a>
              </li>

              <li> <a href="#">
                <div class="step">
                  <div class="line"></div>
                  <div class="circle">3</div>
                </div>
                <span>Payment</span> </a>
              </li>
              <li> <a href="#">
                <div class="step">
                  <div class="line"></div>
                  <div class="circle">4</div>
                </div>
                <span>Order Complete</span> </a>
              </li>
              <li>
                <div class="step">
                  <div class="line"></div>
                </div>
              </li>
            </ul>
            <hr>
          </div>
          <div class="checkout-content" >
            <div class="row">
              <div class="col-xs-12">
                <div class="heading-part align-center">
                  <h2 class="heading">Please fill up your Shipping details</h2>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-6 col-md-8 col-sm-8 col-lg-offset-3 col-sm-offset-2">
                <form action="{{ route('shipping.create') }}" method="POST" id="shipping_form" class="main-form full">
                  @csrf
                  <input type="hidden" name="invoice_id" value="{{ $invoice_id }}">
                  <div class="mb-20">
                    <div class="">
                      <div class="row">
                        <div class="col-xs-12 mb-20">
                          <div class="heading-part">
                            <h3 class="sub-heading">Shipping Address</h3>
                          </div>
                          <hr>
                        </div>

                        <div class="col-xs-12">
                          @if(session()->has('message'))
                            <div class="alert alert-success">
                              {{ session()->get('message') }}
                            </div>
                          @endif
                        </div>


                        <div class="col-sm-6">
                          <div class="input-box {{ $errors->has('country_id') ? ' has-error' : '0' }}">
                            <select name="country_id" id="country_id" class="" required>
                              <option selected="" value="0">Select Country</option>
                              @foreach ($countries as $key => $country)
                                <option value="{{ $country->id }}">{{ $country->name }}</option>
                              @endforeach
                            </select>
                          </div>
                          @if ($errors->has('country_id'))
                            <span class="invalid-feedback">
                              <strong>{{ $errors->first('country_id') }}</strong>
                            </span>
                          @endif
                        </div>

                        <div class="col-sm-6">
                          <div class="input-box {{ $errors->has('state_id') ? ' has-error' : '' }}">
                            <select name="state_id" id="state_id" class="" required>
                              <option value="">Select a State</option>

                            </select>
                          </div>
                          @if ($errors->has('state_id'))
                            <span class="invalid-feedback">
                              <strong>{{ $errors->first('state_id') }}</strong>
                            </span>
                          @endif
                        </div>

                        <div class="col-sm-6">
                          <div class="input-box {{ $errors->has('city') ? ' has-error' : '' }}">
                            <input type="text" id="city" name="city" class="" required placeholder="Select City">
                          </div>
                          @if ($errors->has('city'))
                            <span class="invalid-feedback">
                              <strong>{{ $errors->first('city') }}</strong>
                            </span>
                          @endif
                        </div>

                        <div class="col-sm-6">
                          <div class="input-box {{ $errors->has('postal_code') ? ' has-error' : '' }}">
                            <input type="text" class="" id="postal_code" name="postal_code" required placeholder="Postcode/zip">
                          </div>
                          @if ($errors->has('postal_code'))
                            <span class="invalid-feedback">
                              <strong>{{ $errors->first('postal_code') }}</strong>
                            </span>
                          @endif
                        </div>

                        <div class="col-sm-12">
                          <div class="input-box {{ $errors->has('address') ? ' has-error' : '' }}">
                            <input type="text" class="" id="address" name="address" required placeholder="Enter your address">
                          </div>
                          @if ($errors->has('address'))
                            <span class="invalid-feedback">
                              <strong>{{ $errors->first('address') }}</strong>
                            </span>
                          @endif
                        </div>

                        <div class="col-sm-12">
                          @if(UserHelper::profileComplete())
                          <div class="check-box">
                            <span>
                              <input type="checkbox" class="checkbox" id="registration_address" name="registration_address">
                              <label for="registration_address">Use my registration address as my shipping address</label>
                            </span>
                          </div>
                          @endif
                        </div>
                        <div class="col-sm-12"> <button type="submit" class="btn btn-color right-side">Next</button> </div>
                      </div>
                    </div>
                  </div>

                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- CONTAINER END -->
@endsection
@section('customcss')
  <style media="screen">
    .disabled{
      background-color: #e6e6e6 !important;
    }
    .enable{
      background-color: #ffffff !important;
    }
  </style>
@endsection
@section('custom-js')
  <script type="text/javascript">
    $("#country_id").change(function (){
      $('#state_id').children('option:not(:first)').remove();
      let country = $(this).val();
      $.ajax({
        type: 'POST',
        url: '{{ url('/states') }}',
        data: {
          '_token': $('input[name=_token]').val(),
          'country': country
        },

        success: function(data) {
          //console.log(data);
          data.forEach(function(state){
            $('#state_id').append($("<option></option>").attr("value", state.id).text(state.name));
          });
        }
      });
    });
    $(document).ready(function() {
      $('#registration_address').on( "click", function() {
        let check = document.getElementById(this.id).checked;
        let elements = document.getElementById('shipping_form').elements;
        let colors = ['enable', 'disabled'];
        let remove = ['disabled', 'enable'];
        for(let i = 0, element; element = elements[i++];){
          if(element.type == 'text' || element.type == 'select-one'){
            element.disabled = check
            $('#'+element.id).removeClass(remove[+check]);
            $('#'+element.id).addClass(colors[+check]);
          }
        }
      });

    });
  </script>
@endsection
