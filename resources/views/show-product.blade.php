@extends('layouts.application')
@section('content')
  <!-- Sidebar Mobile -->
  @include('contents.sidebar-menu-mobile')
  <!-- ./Sidebar Mobile -->
  <!-- CONTAIN START -->
  <section class="pt-95">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-xs-12 col-md-9 col-md-offset-2">
          <div class="row">
            <div class="col-md-5 col-sm-5 mb-xs-30">
              <div class="fotorama" data-nav="thumbs" data-allowfullscreen="native">
                <a href="#"><img src="http://grunem-marketplace.s3.amazonaws.com/market-place/products/{{ $product->id ."/". $product->cover_image }}" alt=""></a>
                @foreach ($images as $key => $image)
                  <img src="http://grunem-marketplace.s3.amazonaws.com/{{ $image }}" alt="">
                @endforeach

              </div>
            </div>
            <div class="col-md-7 col-sm-7">
              <div class="row">
                <div class="col-xs-12">
                  <div class="product-detail-main">
                    <div class="product-item-details">
                      <input type="hidden" id="prod_name_{{$product->id}}" name="" value="{{ $product->name }}">

                      @if($discount != NULL)
                        <input type="hidden" id="prod_price_{{$product->id}}" name="" value="{{ $product->price - ($product->price * $discount) }}">
                      @else
                        <input type="hidden" id="prod_price_{{$product->id}}" name="" value="{{ $product->price }}">
                      @endif
                      <input type="hidden" id="prod_color_{{$product->id}}" name="" value="{{ $product->color_id }}">
                      <input type="hidden" id="prod_weight_{{$product->id}}" name="" value="{{ $product->weight }}">
                      {{-- <input type="hidden" id="prod_quantity_{{$product->id}}" name="" value="1"> --}}
                      <input type="hidden" id="prod_image_{{$product->id}}" name="" value="http://grunem-marketplace.s3.amazonaws.com/market-place/products/{{ $product->id ."/". $product->cover_image }}">
                      <h1 class="product-item-name">{{ $product->name }}</h1>
                      <div class="rating-summary-block">
                        <div title="90%" class="rating-result"> <span style="width:90%"></span> </div>
                      </div>
                      <div class="price-box">
                        @if($discount != NULL)
                          <del class="price old-price">${{ $product->price }}</del>
                          <span class="price">${{ ProductHelper::round_up(($product->price - ($product->price * $discount)), 2) }}</span>
                        @else
                          <span class="price">${{ $product->price }}</span>
                        @endif
                      </div>
                      <div class="product-info-stock-sku">
                        <div>
                          <label>Availability: </label>
                          <span class="info-deta">
                            @if($product->stock > 0)
                              In stock({{ $product->stock }})
                            @else
                              Sold out
                            @endif
                          </span>
                        </div>
                        <div>
                          {{-- <label>SKU: </label>
                          <span class="info-deta">20MVC-18</span>  --}}
                        </div>
                      </div>
                      <p>{!! $product->short_description !!}</p>
                      <div class="product-size select-arrow mb-20 mt-30">

                      </div>
                      <div class="product-color select-arrow mb-20">
                        <label>Color: </label>
                        {{ $color->name }}
                        {{-- <select class="selectpicker form-control" id="select-by-color">
                          @foreach ($colors as $key => $color)
                            <option value="{{ $color->id }}">{{ $color->name }}</option>
                          @endforeach
                        </select> --}}
                      </div>
                      <div class="mb-20">
                        <div class="product-qty">
                          <label for="qty">Qty:</label>
                          <div class="custom-qty">
                            <button onclick="var result = document.getElementById('prod_quantity_{{$product->id}}'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 1 ) result.value--;return false;" class="reduced items" type="button"> <i class="fa fa-minus"></i> </button>
                            <input type="text" class="input-text qty" title="Qty" value="1" maxlength="8" id="prod_quantity_{{$product->id}}" name="qty">
                            <button onclick="var result = document.getElementById('prod_quantity_{{$product->id}}'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="increase items" type="button"> <i class="fa fa-plus"></i> </button>
                          </div>
                        </div>
                        <div class="bottom-detail cart-button">
                          <ul>
                            <li class="pro-cart-icon">

                              <button title="Add to Cart" id="{{ $product->id }}" class="btn-color add-to-cart"><span></span>Add to Cart</button>

                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="bottom-detail">
                        <ul>
                          <li class="pro-wishlist-icon"><a href="wishlist.html"><span></span>Wishlist</a></li>
                          <li class="pro-compare-icon"><a href="compare.html"><span></span>Compare</a></li>
                          <li class="pro-email-icon"><a href="#"><span></span>Email to Friends</a></li>
                        </ul>
                      </div>
                      <div class="share-link">
                        <label>Share This : </label>
                        <div class="social-link">
                          <ul class="social-icon">
                            <li><a class="facebook" title="Facebook" href="#"><i class="fa fa-facebook"> </i></a></li>
                            <li><a class="twitter" title="Twitter" href="#"><i class="fa fa-twitter"> </i></a></li>
                            <li><a class="linkedin" title="Linkedin" href="#"><i class="fa fa-linkedin"> </i></a></li>
                            <li><a class="rss" title="RSS" href="#"><i class="fa fa-rss"> </i></a></li>
                            <li><a class="pinterest" title="Pinterest" href="#"><i class="fa fa-pinterest"> </i></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {{-- <div class="col-md-3">
          <div class="brand-logo-pro align-center mb-30">
            <img src="images/brand5.png" alt="">
          </div>
          <div class="sub-banner-block align-center">
            <img src="images/pro-banner.jpg" alt="">
          </div>
        </div> --}}
      </div>
    </div>
  </section>

  <section class="ptb-95">
    <div class="container">
      <div class="product-detail-tab">
        <div class="row">
          <div class="col-md-12">
            <div id="tabs">
              <ul class="nav nav-tabs">
                <li><a class="tab-Description selected" title="Description">Description</a></li>
                <li><a class="tab-Product-Tags" title="Product-Tags">Specification</a></li>
                {{-- <li><a class="tab-Reviews" title="Reviews">Reviews</a></li> --}}
              </ul>
            </div>
            <div id="items">
              <div class="tab_content">
                <ul>
                  <li>
                    <div class="items-Description selected ">
                      <div class="Description">
                        <p> {!! $product->long_description !!} </p>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div class="items-Product-Tags">
                      <p>{!! $product->specifications !!}</p>
                    </div>
                  </li>

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('customcss')
  <style media="screen">
    .d-flex{
      display: flex;
    }
    .f-center{
      justify-content: center;
    }
  </style>
@endsection
