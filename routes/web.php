<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index');

Route::get('/customers/new', 'CustomersController@new');

Auth::routes();
Route::get('/register/{sponsor}', 'Auth\RegisterController@showRegistrationForm');

Route::get('/home', 'MainController@index')->name('home');
Route::get('/product/{product_id}', 'ProductsController@show');
Route::get('/products/category/{category_id}/{limit?}', 'ProductsController@productsByCategory');
Route::post('/products/category/paginate', 'ProductsController@productsByCategory');
Route::post('/products/category/filtered', 'ProductsController@productsByCategory');
Route::post('/products/category/limit', 'ProductsController@productsByCategory');

Route::get('/order/overview', 'InvoicesController@renderOverView');
/*Group auth*/
Route::post('/order/create', 'InvoicesController@store')->name('invoice.create');
Route::middleware(['auth'])->group(function () {
  Route::get('/order/shipping/{invoice_id}', 'ShippingController@new');
  Route::post('/shipping/create', 'ShippingController@store')->name('shipping.create');
  Route::get('/order/payment/{invoice_id}', 'PaymentsController@new');
  Route::post('/order/payment/create', 'PaymentsController@store')->name('payment.create');
  Route::get('/order/order-complete/{invoice_id}', 'PaymentsController@orderComplete');

  Route::get('/user/account', 'UsersController@account');
  Route::post('/user/account/update-address', 'UsersController@updateShippingAddress');

  Route::post('/user/wishlist/create', 'WishlistController@create');
  Route::post('/user/wishlist/create', 'WishlistController@delete');
});
/*End Group auth*/
Route::get('/search-products', 'ProductsController@search')->name('search.products');
/*Get states*/
Route::post('/states', 'StatesController@getStatesByCountry');

Route::get('/verify-user/{code}', 'Auth\RegisterController@activateUser')->name('activate.user');
Route::get('/user/log-out', 'CustomersController@logOut')->name('user.logout');



/*
  External request
*/
Route::post('/user/grunem/create-new-user', 'UserApiController@registerUserFromGrunem');
